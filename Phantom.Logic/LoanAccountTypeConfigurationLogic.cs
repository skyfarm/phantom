﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using Phantom.Data;

namespace Phantom.Logic
{
    public class LoanAccountTypeConfigurationLogic : Repository<LoanAccountTypeConfiguration>
    {
        //ONCE CREATED, YOU SHOULD NOT ADD A NEW CONFIG, ONLY UPDATE
        public void Insert(LoanAccountTypeConfiguration config)
        {
            AddNewTimeStamps(config);
            LoanAccountTypeConfigurationDAO lacDAO = new LoanAccountTypeConfigurationDAO();
            lacDAO.InsertNewLoanAccountTypeConfiguration(config);
        }
        public void Update(LoanAccountTypeConfiguration config)
        {
            AddDateModifiedTimeStamp(config);
            LoanAccountTypeConfigurationDAO lacDAO = new LoanAccountTypeConfigurationDAO();
            lacDAO.UpdateLoanAccountTypeConfiguration(config);
        }
        public LoanAccountTypeConfiguration GetLoanAccountTypeConfiguration()
        {
            LoanAccountTypeConfigurationDAO lacDAO = new LoanAccountTypeConfigurationDAO();
            return lacDAO.RetreiveLoanAccountTypeConfiguration(1);
        }
    }
}
