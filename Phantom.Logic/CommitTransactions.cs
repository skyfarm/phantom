﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Data;
using Phantom.Core;

namespace Phantom.Logic
{
    public class CommitTransactions
    {
        public static void CommitAll()
        {
            RepositoryDAO<Entity> commit = new RepositoryDAO<Entity>();
            commit.Commit();
        }
    }
}
