﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using Phantom.Data;

namespace Phantom.Logic
{
    public class CustomerLogic:Repository<Customer>
    {
        public bool CheckIfCustomerExists(string phoneNumber, string email)
        {
            return new CustomerDAO().CheckExistingCustomer(phoneNumber, email);
        }
    }
}
