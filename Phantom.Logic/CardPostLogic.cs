﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;

namespace Phantom.Logic
{
    public class CardPostLogic : Repository<CardPost>
    {
        public bool IsFundsSufficientInCustomerAccount(CustomerAccount account, decimal amount)
        {
            if (account.AccountBalance >= amount)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool IsFundsSufficientInTillAccount( decimal amount)
        {
            //THERE IS ONLY ONE TILL ACCOUNT FOR CARD CASH WITHDRAWALS (ATM)
            //UNTIL I FIGURE OUT HOW TO GET THE BRANCH ID FROM THE SIMULATOR
            //BUT THERE'S NO TIME FOR THAT ATM (At The Moment) GET IT? LOL
            //I'M AWESOME....

            GLAccount tillAccount = new GLAccountLogic().GetByID(1024);
            if (tillAccount.AccountBalance >= amount)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public void CashWithdrawalPost(CustomerAccount customerAccount, decimal amount, decimal fee = 0)
        {
            GLAccountLogic glaLogic = new GLAccountLogic();

            GLAccount ATMTillAccount = new GLAccountLogic().GetByID(1024);
            ATMTillAccount.AccountBalance -= amount;
            glaLogic.Update(ATMTillAccount);

            GLAccount cardTransactionFeeIncomeGL = new GLAccountLogic().GetByID(1025);
            cardTransactionFeeIncomeGL.AccountBalance += fee;
            glaLogic.Update(cardTransactionFeeIncomeGL);

            GLAccount accountsPayable = glaLogic.GetByID(1020);
            accountsPayable.AccountBalance -= (amount + fee);
            glaLogic.Update(accountsPayable);

            GLAccount allIncome = glaLogic.GetByID(1017);
            allIncome.AccountBalance += fee;
            glaLogic.Update(allIncome);


            customerAccount.AccountBalance -= (amount + fee);
            new CustomerAccountLogic().Update(customerAccount);

        }
        public void FundsTransferPost(CustomerAccount fromAccount, CustomerAccount toAccount, decimal amount, decimal fee = 0)
        {
            GLAccountLogic glaLogic = new GLAccountLogic();

            GLAccount cardTransactionFeeIncomeGL = new GLAccountLogic().GetByID(1025);
            cardTransactionFeeIncomeGL.AccountBalance += fee;
            glaLogic.Update(cardTransactionFeeIncomeGL);

            GLAccount allIncome = glaLogic.GetByID(1017);
            allIncome.AccountBalance += fee;
            glaLogic.Update(allIncome);

            GLAccount accountsPayable = glaLogic.GetByID(1020);
            accountsPayable.AccountBalance -= fee;
            glaLogic.Update(accountsPayable);

            CustomerAccountLogic caLogic = new CustomerAccountLogic();
            fromAccount.AccountBalance -= (amount + fee);
            toAccount.AccountBalance += amount;

            caLogic.Update(fromAccount);
            caLogic.Update(toAccount);
        }
    }
}
