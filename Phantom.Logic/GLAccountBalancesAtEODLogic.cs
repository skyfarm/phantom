﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using Phantom.Data;

namespace Phantom.Logic
{
    public class GLAccountBalancesAtEODLogic : Repository<GLAccountBalancesAtEOD>
    {
        public List<GLAccountBalancesAtEOD> GetAllByCategory(AccountCategory category, DateTime finDate)
        {
            return new GLAccountBalancesAtEODDAO().RetreiveByCategory(category,finDate);
        }
    }
}
