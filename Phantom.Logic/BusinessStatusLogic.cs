﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;

namespace Phantom.Logic
{
    public class BusinessStatusLogic : Repository<BusinessStatus>
    {
        
        public static bool OpenBusiness()
        {
            //if business is already opened dont open 
            BusinessStatusLogic bsLogic = new BusinessStatusLogic();
            BusinessStatus status = bsLogic.GetByID(1);

            if (status.Status == StateOfBusiness.Open)
            {
                //already open
                return false;
            }
            else
            {
                //open business
                status.Status = StateOfBusiness.Open;
                bsLogic.Update(status);
                CommitTransactions.CommitAll();
                return true;
            }
        }
        public static bool CloseBusiness()
        {
        //    //if business is already closed dont do anything 
            BusinessStatusLogic bsLogic = new BusinessStatusLogic();
            BusinessStatus status = bsLogic.GetByID(1);

            if (status.Status == StateOfBusiness.Closed)
            {
        //        //already closed
                return false;
            }
            else
            {
        //       //close business
               status.Status = StateOfBusiness.Closed;
               bsLogic.Update(status);

        //        //do all the calculations here

               //SAVINGS
               //Get Config

               SavingsAccountTypeConfigurationLogic satLogic = new SavingsAccountTypeConfigurationLogic();
               SavingsAccountTypeConfiguration sConfig = satLogic.GetByID(1);


               //Get all savings accounts
               CustomerAccountLogic caLogic = new CustomerAccountLogic();
               List<CustomerAccount> savingsAccounts = caLogic.GetAllActiveCustomerAccountOfType(AccountType.Savings);

               GLAccountLogic glaLogic = new GLAccountLogic();

               foreach (CustomerAccount savingsAccount in savingsAccounts)
               {
                   decimal interest = CalculateDailyInterest(savingsAccount.AccountBalance, sConfig.CreditInterestRate);
                   GLAccount interestExpenseGL = sConfig.InterestExpenseGLAccount;

                   savingsAccount.AccountBalance += interest;
                   caLogic.Update(savingsAccount);

                   interestExpenseGL.AccountBalance += interest;
                   glaLogic.Update(interestExpenseGL);

                   GLAccount accountsPayable = glaLogic.GetByID(1020);
                   accountsPayable.AccountBalance += interest;
                   glaLogic.Update(accountsPayable);

                   GLAccount allExpense = glaLogic.GetByID(1018);
                   allExpense.AccountBalance += interest;
                   glaLogic.Update(allExpense);

               }



               //current
               //Get Config

               CurrentAccountTypeConfigurationLogic catLogic = new CurrentAccountTypeConfigurationLogic();
               CurrentAccountTypeConfiguration cConfig = catLogic.GetByID(1);

               List<CustomerAccount> currentAccounts = caLogic.GetAllActiveCustomerAccountOfType(AccountType.Current);
               foreach (CustomerAccount currentAccount in currentAccounts)
               {
                   decimal interest = CalculateDailyInterest(currentAccount.AccountBalance, cConfig.CreditInterestRate);
                   GLAccount interestExpenseGL = cConfig.InterestExpenseGLAccount;

                   currentAccount.AccountBalance += interest;
                   caLogic.Update(currentAccount);

                   interestExpenseGL.AccountBalance += interest;
                   glaLogic.Update(interestExpenseGL);

                   GLAccount accountsPayable = glaLogic.GetByID(1020);
                   accountsPayable.AccountBalance += interest;
                   glaLogic.Update(accountsPayable);

                   GLAccount allExpense = glaLogic.GetByID(1018);
                   allExpense.AccountBalance += interest;
                   glaLogic.Update(allExpense);

               }


               //loan
               //get config
               LoanAccountTypeConfigurationLogic latLogic = new LoanAccountTypeConfigurationLogic();
               LoanAccountTypeConfiguration lConfig = latLogic.GetByID(1);


               List<CustomerAccount> loanAccounts = caLogic.GetAllActiveCustomerAccountOfType(AccountType.Loan);
               foreach (CustomerAccount loanAccount in loanAccounts)
               {
                   decimal interest = CalculateDailyInterest(loanAccount.AccountBalance, lConfig.DebitInterestRate);
                   GLAccount interestIncomeGL = lConfig.InterestIncomeGLAccount;

                   loanAccount.AccountBalance -= interest;
                   caLogic.Update(loanAccount);

                   interestIncomeGL.AccountBalance += interest;
                   glaLogic.Update(interestIncomeGL);

                   GLAccount accountsPayable = glaLogic.GetByID(1020);
                   accountsPayable.AccountBalance -= interest;
                   glaLogic.Update(accountsPayable);

                   GLAccount allIncome = glaLogic.GetByID(1017);
                   allIncome.AccountBalance += interest;
                   glaLogic.Update(allIncome);

               }


               FinancialDateLogic fdLogic = new FinancialDateLogic();
               FinancialDate finDate = fdLogic.GetByID(1);

               GLAccountBalancesAtEODLogic glEODAccBalLogic = new GLAccountBalancesAtEODLogic();
               decimal retainedIncome = glaLogic.GetByID(1017).AccountBalance - glaLogic.GetByID(1018).AccountBalance;

               GLAccount retainedIncomeGL = glaLogic.GetByID(1022);
               retainedIncomeGL.AccountBalance = retainedIncome;
               glaLogic.Update(retainedIncomeGL);

               GLAccount retainedIncomeAssetGL = glaLogic.GetByID(1023);
               retainedIncomeAssetGL.AccountBalance = retainedIncome;
               glaLogic.Update(retainedIncomeAssetGL);

               List<GLAccount> allGLAccounts = glaLogic.GetAll();
               foreach (GLAccount glAccount in allGLAccounts)
               {
                   GLAccountBalancesAtEOD glEODAccBal = new GLAccountBalancesAtEOD()
                   {
                       GLAccount = glAccount,
                       AccountBalanceAtEOD = glAccount.AccountBalance,
                       FinancialDate = finDate.CurrentFinancialDate
                   };

                   glEODAccBalLogic.Insert(glEODAccBal);

               }

               DateTime newFinancialDate = finDate.CurrentFinancialDate.AddDays(1);
               finDate.CurrentFinancialDate = newFinancialDate;

               fdLogic.Update(finDate);



               CommitTransactions.CommitAll();
                return true;
            }
        }

        private static decimal CalculateDailyInterest(decimal principal, decimal rate)
        {
            return ((principal * rate) / 36500);
        }
    }
}
