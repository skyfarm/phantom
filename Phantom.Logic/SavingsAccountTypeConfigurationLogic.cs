﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using Phantom.Data;

namespace Phantom.Logic
{
    public class SavingsAccountTypeConfigurationLogic : Repository<SavingsAccountTypeConfiguration>
    {
        //ONCE CREATED, YOU SHOULD NOT ADD A NEW CONFIG, ONLY UPDATE
        //public void Insert(SavingsAccountTypeConfiguration config)
        //{
        //    AddNewTimeStamps(config);
        //    SavingsAccountTypeConfigurationDAO sacDAO = new SavingsAccountTypeConfigurationDAO();
        //    sacDAO.InsertNewSavingsAccountTypeConfiguration(config);
        //}
        public void Update(SavingsAccountTypeConfiguration config)
        {
            AddDateModifiedTimeStamp(config);
            SavingsAccountTypeConfigurationDAO sacDAO = new SavingsAccountTypeConfigurationDAO();
            sacDAO.UpdateSavingsAccountTypeConfiguration(config);
        }
        public SavingsAccountTypeConfiguration GetSavingsAccountTypeConfiguration()
        {
            SavingsAccountTypeConfigurationDAO sacDAO = new SavingsAccountTypeConfigurationDAO();
            return sacDAO.RetreiveSavingsAccountTypeConfiguration(1);
        }
    }
}
