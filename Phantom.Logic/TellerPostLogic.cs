﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using Phantom.Data;

namespace Phantom.Logic
{
    public class TellerPostLogic : Repository<TellerPost>
    {
       
        public bool IsFundsSufficientInCustomerAccount(CustomerAccount account, decimal amount)
        {
            if (account.AccountBalance >= amount)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        public bool IsFundsSufficientInTillAccount(GLAccount account, decimal amount)
        {
            if (account.AccountBalance >= amount)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public void DepositPost(GLAccount tillAccount, CustomerAccount customerAccount, decimal amount)
        {
            customerAccount.AccountBalance += amount;
            tillAccount.AccountBalance += amount;
           // account.AccountBalance -= amount;
           
            GLAccountLogic glaLogic = new GLAccountLogic();

            GLAccount accountsPayable = glaLogic.GetByID(1020);
            accountsPayable.AccountBalance += amount;

            glaLogic.Update(tillAccount);
            glaLogic.Update(accountsPayable);

            CustomerAccountLogic caLogic = new CustomerAccountLogic();
            caLogic.Update(customerAccount);
            
        }
        public void WithdrawPost(GLAccount tillAccount, CustomerAccount customerAccount, decimal amount)
        {
            if (customerAccount.AccountType == AccountType.Current )
            {
                //do COT
                CurrentAccountTypeConfigurationLogic catLogic = new CurrentAccountTypeConfigurationLogic();
                CurrentAccountTypeConfiguration config = catLogic.GetByID(1);

                decimal cotValue = (config.COT / 100) * amount;

                customerAccount.AccountBalance -= cotValue;

                GLAccountLogic cotGlaLogic = new GLAccountLogic();
                GLAccount COTIncomeGL = cotGlaLogic.GetByID(config.COTIncomeGLAccount.Id);
                COTIncomeGL.AccountBalance += cotValue;
                cotGlaLogic.Update(COTIncomeGL);

                GLAccount allIncome = cotGlaLogic.GetByID(1017);
                allIncome.AccountBalance += cotValue;
                cotGlaLogic.Update(allIncome);


                GLAccount accountPayable = cotGlaLogic.GetByID(1020);
                accountPayable.AccountBalance -= cotValue;
                cotGlaLogic.Update(accountPayable);

            }

            customerAccount.AccountBalance -= amount;
            tillAccount.AccountBalance -= amount;
          

            GLAccountLogic glaLogic = new GLAccountLogic();
            glaLogic.Update(tillAccount);

            GLAccount accountsPayable = glaLogic.GetByID(1020);
            accountsPayable.AccountBalance -= amount;
            glaLogic.Update(accountsPayable);

            CustomerAccountLogic caLogic = new CustomerAccountLogic();
            caLogic.Update(customerAccount);

        }
    }
}
