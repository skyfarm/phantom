﻿using Phantom.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Logic
{
    public class NuggetLogic : Repository<Nugget>
    {
        Random random = new Random();
        public Nugget GetRandomNugget()
        {
            int count = (int) Count();

            int randomNugget = random.Next(1, count + 1);

            return GetByID(randomNugget);
        }
    }
}
