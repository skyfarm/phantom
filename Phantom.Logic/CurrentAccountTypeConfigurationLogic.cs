﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using Phantom.Data;

namespace Phantom.Logic
{
    public class CurrentAccountTypeConfigurationLogic : Repository<CurrentAccountTypeConfiguration>
    {
        public void Insert(CurrentAccountTypeConfiguration config)
        {
            AddNewTimeStamps(config);
            CurrentAccountTypeConfigurationDAO cacDAO = new CurrentAccountTypeConfigurationDAO();
            cacDAO.InsertNewCurrentAccountTypeConfiguration(config);
        }
        public void Update(CurrentAccountTypeConfiguration config)
        {
            AddDateModifiedTimeStamp(config);
            CurrentAccountTypeConfigurationDAO cacDAO = new CurrentAccountTypeConfigurationDAO();
            cacDAO.UpdateCurrentAccountTypeConfiguration(config);
        }
        public CurrentAccountTypeConfiguration GetCurrentAccountTypeConfiguration()
        {
            CurrentAccountTypeConfigurationDAO cacDAO = new CurrentAccountTypeConfigurationDAO();
            return cacDAO.RetreiveCurrentAccountTypeConfiguration(1);
        }
    }
}
