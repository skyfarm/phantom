﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Data;
using Phantom.Core;

namespace Phantom.Logic
{
    public class BranchLogic:Repository<Branch>
    {
        public void Insert(Branch branch)
        {
            //generate code somewhere here
            int branchCode = GenerateCode();

            branch.Code = branchCode.ToString();


           // branch.DateCreated = DateTime.Now;
            //branch.DateModified = DateTime.Now;

            AddNewTimeStamps(branch);
            BranchDAO bDAO = new BranchDAO();
            bDAO.Insert(branch);
        }
       
        public Branch GetBranchDetails(int id)
        {
            BranchDAO bDAO = new BranchDAO();
            return bDAO.GetBranchDetailsByID(id);
        }
        public long GetCount()
        {
            return Count();
        }
    }
}
