﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using Phantom.Data;

namespace Phantom.Logic
{
    public class CustomerAccountLogic : Repository<CustomerAccount>
    {

        public void CreateAccount(CustomerAccount account)
        {
            account.AccountNumber = GenerateAccountNumber(account);
            Insert(account);
        }
        public void CreateLoan(CustomerAccount account, Loan loan)
        {
            account.AccountNumber = GenerateAccountNumber(account);
            AddNewTimeStamps(account);
            CustomerAccountDAO caDAO = new CustomerAccountDAO();
            caDAO.InsertNewCustomerAccount(account);


            loan.CustomerAccount = account;
            LoanLogic lLogic = new LoanLogic();
            lLogic.Insert(loan);
        }
        //public void Update(CustomerAccount account)
        //{
        //    AddDateModifiedTimeStamp(account);
        //    CustomerAccountDAO caDAO = new CustomerAccountDAO();
        //    caDAO.UpdateExistingCustomerAccount(account);
        //}
        public string GenerateAccountNumber(CustomerAccount account) 
        {
            int type = (int) account.AccountType;
            string typeString = type.ToString();
            int customerID = account.Customer.Id;
            string paddedCustomerID = customerID.ToString("D3");
            string randomNumber = GenerateCode().ToString();

            /*Account Number consists of AccountType (savings/current/loan), the customer ID, then a random number*/
            StringBuilder accountNumber = new StringBuilder(typeString);
            accountNumber.Append(paddedCustomerID);
            accountNumber.Append(randomNumber);

            return accountNumber.ToString();
        }
        public List<CustomerAccount> GetACustomersAccounts(Customer customer)
        {
            CustomerAccountDAO caDAO = new CustomerAccountDAO();
            return caDAO.RetrieveACustomersAccounts(customer);
        }

        public List<CustomerAccount> GetActiveCustomerAccountsInABranch(Branch branch)
        {
            CustomerAccountDAO caDAO = new CustomerAccountDAO();
            return caDAO.RetrieveCustomerAccountsInABranch(branch);
        }

        public List<CustomerAccount> GetAllActiveCustomerAccounts()
        {
            CustomerAccountDAO caDAO = new CustomerAccountDAO();
            return caDAO.RetreiveAllActiveCustomerAccounts();
        }

        public List<CustomerAccount> GetAllActiveCustomerAccountOfType(AccountType type)
        {
            CustomerAccountDAO caDAO = new CustomerAccountDAO();
            return caDAO.RetreiveAllActiveCustomerAccountsOfType(type);
        }
        public CustomerAccount GetByAccountNumber(string accountNumber)
        {
            return new CustomerAccountDAO().RetreiveByAccountNumber(accountNumber);
        }
        public List<CustomerAccount> GetCustomerAccountsWithCards()
        {
            return new CustomerAccountDAO().RetrieveAllCustomerAccountsWithCards();
        }
        public List<CustomerAccount> GetCustomerAccountsWithoutCards()
        {
            return new CustomerAccountDAO().RetrieveAllCustomerAccountsWithoutCards();
        }
    }
}
