﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using Phantom.Data;

namespace Phantom.Logic
{
    public class CardLogic : Repository<Card>
    {
        private const string BIN = "51989"; //DO  NOT CHANGE!!!
        public void GenerateCardDetails(Card card)
        {
            card.CardPAN = GenerateCardPAN();
            card.CVV = GenerateCVV();
            card.ExpiryDate = GetExpiryDate();

            //Insert(card);
        }
        public List<Card> GetAllMasked()
        {
            List<Card> list = GetAll();
            
            foreach (Card card in list)
            {
                card.CardPAN = card.CardPAN.Remove(0, 12);
                card.CardPAN = card.CardPAN.Insert(0, "************");
                card.CVV = "***";
            }
            return list;
        }
        private string GenerateCardPAN()
        {
            //card pan is 16 digits
            //bin takes the first 5 digits
            //remaining 11 cannot be generated once
            //so its split into two
            Random rand1 = new Random();
            int generated1 = rand1.Next(100000, 999999); //6digits
            Random rand2 = new Random();
            int generated2 = rand2.Next(10000, 99999); //5digits
            string generatedString = generated1.ToString() + generated2.ToString();
            string cardPAN = BIN + generatedString;
            return cardPAN;
        }
        private string GenerateCVV()
        {
            Random rand = new Random();
            int generated = rand.Next(100, 999);
            return generated.ToString();
        }
        private DateTime GetExpiryDate()
        {
            return DateTime.Now.AddYears(2);
        }
        public bool CheckIfCardExist(string cardPAN, out Card card)
        {
            return new CardDAO().CheckCard(cardPAN, out card);
        }
    }
}
