﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using Phantom.Data;

namespace Phantom.Logic
{
    public class GLAccountLogic : Repository<GLAccount>
    {
        public void GenerateGLACodeAndInsert(GLAccount glAccount)
        {
            glAccount.Code =  GenerateGLACode(glAccount.Category);
            Insert(glAccount);
        }
    
        public void SetGLAccountToAssigned(GLAccount glAccount) 
        {
            glAccount.IsAssigned = true;
            Update(glAccount);
        }
        public string GenerateGLACode(GLCategory category)
        {
            //generate code based on category
            int intPrefix = (int)category.Category;
            string prefix = intPrefix.ToString();
            StringBuilder glaCode = new StringBuilder(prefix);
            string code = GenerateCode().ToString();
            glaCode.Append(code);
            return glaCode.ToString();
        }
        public List<GLAccount> GetAllUnAssignedGLAccounts() 
        {
            GLAccountDAO glaDAO = new GLAccountDAO();
            return glaDAO.RetreiveALLUnassignedGLAccounts();
        }
        public List<GLAccount> GetAllUnAssignedGLAccounts(Branch branch)
        {
            GLAccountDAO glaDAO = new GLAccountDAO();
            return glaDAO.RetreiveALLUnassignedGLAccountsInABranch(branch);
        }
       
    }
}
