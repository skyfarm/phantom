﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using Phantom.Data;

namespace Phantom.Logic
{
    public class GLPostLogic : Repository<GLPost>
    {
        public void ProcessPost()
        {

        }
        public bool IsFundsSufficient(GLAccount account, decimal amount)
        {
            if (account.AccountBalance >= amount)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        public void DebitAccount(GLAccount account, decimal amount)
        {
            account.AccountBalance -= amount;
            GLAccountLogic glaLogic = new GLAccountLogic();
            glaLogic.Update(account);
           
        }
        public void CreditAccount(GLAccount account, decimal amount)
        {
            account.AccountBalance += amount;
            GLAccountLogic glaLogic = new GLAccountLogic();
            glaLogic.Update(account);
        }
    }
}
