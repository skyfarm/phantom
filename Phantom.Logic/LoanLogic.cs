﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using Phantom.Data;

namespace Phantom.Logic
{
    public class LoanLogic : Repository<Loan>
    {
        public void Insert(Loan loan)
        {
            AddNewTimeStamps(loan);
            LoanDAO lDAO = new LoanDAO();
            lDAO.InsertNewLoan(loan);
        }
        public long GetCount()
        {
            return Count();
        }
    }
}
