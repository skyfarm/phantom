﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using Phantom.Data;

namespace Phantom.Logic
{
    public class TellerLogic: Repository<Teller>
    {
        public void RegisterTeller(Teller teller)
        {
            MakeUserATeller(teller.User);
            SetGLAccountToAssigned(teller.TillAccount);
            Insert(teller);
        }
        //public List<Teller> GetAllTellers()
        //{
        //    TellerDAO tDAO = new TellerDAO();
        //    return tDAO.RetrieveAllTellers();
        //}
       
        public void MakeUserATeller(User user) 
        {
            UserLogic uLogic = new UserLogic();
            uLogic.MakeUserATeller(user);
        }
        public void SetGLAccountToAssigned(GLAccount glAccount) 
        {
            GLAccountLogic glaLogic = new GLAccountLogic();
            glaLogic.SetGLAccountToAssigned(glAccount);
        }


        public Teller GetTellerInformationByUserID(User user)
        {
            TellerDAO tDAO = new TellerDAO();
            return tDAO.RetrieveTellerByUserId(user);
            
        }
    }
}
