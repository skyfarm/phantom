﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using Phantom.Data;

namespace Phantom.Logic
{
    public class Repository<T> where T:Entity, new()
    {
        public void AddNewTimeStamps(T entity)
        {
            entity.DateCreated = DateTime.Now;
            entity.DateModified = DateTime.Now;
        }
        public void AddDateModifiedTimeStamp(T entity)
        {
            entity.DateModified = DateTime.Now;
        }
        public int GenerateCode()
        {
            Random rand = new Random();
            return rand.Next(100000, 999999);
        }
        public long Count() 
        {
            RepositoryDAO<T> entity = new RepositoryDAO<T>();
            return entity.GetCount();
        }
        public List<T> GetAll() 
        {
            RepositoryDAO<T> entity = new RepositoryDAO<T>();
            return entity.RetrieveList();   
        }
        public T GetByID(int id)
        {
            RepositoryDAO<T> entity = new RepositoryDAO<T>();
            return entity.RetrieveByID(id);
        }
        public void Insert(T entity)
        {
            AddNewTimeStamps(entity);
            RepositoryDAO<T> eDAO = new RepositoryDAO<T>();
            eDAO.Insert(entity);
        }
        public void Update(T entity)
        {
            AddDateModifiedTimeStamp(entity);
            RepositoryDAO<T> eDAO = new RepositoryDAO<T>();
            eDAO.Update(entity);
        }
        
        //public void Delete(T entity)
        //{

        //}
        //public T RetreiveByID(int id)
        //{
        //    T ent = new T();
        //    return ent;
        //}
        //public T RetrieveAll()
        //{
        //    T ent = new T();
        //    return ent;
        //}
    }
}
