﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using Phantom.Data;
using System.Web.Security;
using System.Net;
using System.Net.Mail;
using System.Net.Configuration;
using System.Configuration;
using System.IO;


namespace Phantom.Logic
{
    public class UserLogic:Repository<User>
    {
        public void GeneratePasswordAndInsert(User user)
        {
            AddNewTimeStamps(user);
            user.Password = Membership.GeneratePassword(8, 1);
            
            Insert(user);
            SenddLoginDetailsToUser(user);
        }
        public bool CheckIfUsernameExists(string username ,string email)
        {
            return new UserDAO().CheckExistingUser(username,email);
        }
        public List<User> GetAllNonTellers()
        {
            UserDAO uDAO = new UserDAO();
            return uDAO.RetrieveAllNonTellers();
        }
        public void MakeUserATeller(User user) 
        {
            user.IsTeller = true;
            Update(user);
        }
       

        public static void SenddLoginDetailsToUser(User user)
        {
            string fromAddress = "phantom_cba@yahoo.com";
            string password = "!@#$%^&*()";

            string subject = "Your Login Details";
            StringBuilder body = new StringBuilder();
            body.Append("Welcome to Phantom!");
            body.AppendLine();
            body.AppendLine("Your Account has been created,");
            body.AppendLine("and here are you login details");
            body.AppendLine("Username : " + user.UserName.ToString());
            body.AppendLine("Password : " + user.Password.ToString());

            string bodyString = body.ToString();

            string toAddress = user.Email.ToString();

            using (MailMessage mailMessage = new MailMessage(fromAddress, toAddress, subject,bodyString)) 
            using(SmtpClient smtp = new SmtpClient())
            {
           
                smtp.Host = "smtp.mail.yahoo.com";
                smtp.EnableSsl = true;
                NetworkCredential networkCredential = new NetworkCredential(fromAddress, password);
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = networkCredential;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Port = 587;
                try
                {
                    smtp.Send(mailMessage);

                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }  
        }
        public bool Authenticate(string username, string password)
        {
            UserDAO uDAO = new UserDAO();
            return uDAO.Authenticate(username, password);
        }

        public User GetUserByLogInDetails(string username, string password)
        {
            UserDAO uDAO = new UserDAO();
            return uDAO.RetreiveUserByLogin(username, password);
        }
       
    }

}
