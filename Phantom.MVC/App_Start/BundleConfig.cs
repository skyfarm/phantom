﻿using System.Web;
using System.Web.Optimization;

namespace Phantom.MVC
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      //"~/Scripts/jquery-1.10.2.js",
                      "~/Scripts/jquery-ui.min.js",
                      "~/Scripts/bootstrap.min.js",
                      //"~/Scripts/jquery.validate.min.js",
                      "~/Scripts/bootstrap-selectpicker.js",
                      "~/Scripts/bootstrap-checkbox-radio-switch-tags.js",
                      "~/Scripts/bootstrap-notify.js",
                      "~/Scripts/sweetalert2.js",
                      "~/Scripts/bootstrap-table.js",
                      "~/Scripts/jquery.datatables.js",                     
                      "~/Scripts/paper-dashboard.js")
                       //"~/Scripts/demo.js")
                       );
                      //"~/Scripts/tables.js",
                      //"~/Scripts/tables.js",
                      //"~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/TableMagic").Include(
                        "~/Scripts/SeunTableScript.js"));
            bundles.Add(new ScriptBundle("~/Script/DemoJS").Include(
                     "~/Scripts/demo.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/paper-dashboard.css",
                      "~/Content/demo.css",
                      //"~/Content/calendar.css",
                      //"~/Content/forms.css",
                      "~/Content/themify-icons.css"));
        }
    }
}
