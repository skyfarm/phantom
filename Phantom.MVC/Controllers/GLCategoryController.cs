﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phantom.Logic;
using Phantom.Core;
using Phantom.MVC.Models;

namespace Phantom.MVC.Controllers
{
    public class GLCategoryController : Controller
    {
        Repository<GLCategory> repository = new Repository<GLCategory>();
        GLCategory category;
        //
        // GET: /GLCategory/
        public ActionResult Index()
        {
            List<GLCategory> categories = repository.GetAll();
            return View(categories);
        }

        //
        // GET: /GLCategory/Details/5
        public ActionResult Details(int id)
        {
            category = repository.GetByID(id);
            if (category == null)
            {
                TempData["message"] = "Category not found!";
                return RedirectToAction("Index");
            }

            return View(category);
        }

        //
        // GET: /GLCategory/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /GLCategory/Create
        [HttpPost]
        public ActionResult Create(GLCategory glCategory)
        {

            if (ModelState.IsValid)
            {               
                repository.Insert(glCategory);
                CommitTransactions.CommitAll();
                TempData["message"] = "Successfully created GL Category";
                return RedirectToAction("Index");
            }
            return View();

        }

        //
        // GET: /GLCategory/Edit/5
        public ActionResult Edit(int id)
        {
            category = repository.GetByID(id);
            if (category == null)
            {
                TempData["message"] = "GL Category not found";
                return RedirectToAction("Index");
            }

            return View(category);

        }

        //
        // POST: /GLCategory/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                category = repository.GetByID(id);
                if (category == null)
                {
                    TempData["message"] = "Category not found!";
                    return RedirectToAction("Index");
                }

                if (ModelState.IsValid)
                {
                    UpdateModel(category);
                    repository.Update(category);
                    CommitTransactions.CommitAll();
                    TempData["message"] = "Successfully updated GL Category";
                    return RedirectToAction("Index");
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /GLCategory/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /GLCategory/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
