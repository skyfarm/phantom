﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phantom.Logic;
using Phantom.Core;
using Phantom.MVC.Models;

namespace Phantom.MVC.Controllers
{
    public class CustomerController : Controller
    {
        Repository<Customer> repository = new Repository<Customer>();
        //
        // GET: /Customer/
        public ActionResult Index()
        {
            List<Customer> customers = repository.GetAll();
            return View(customers);
        }

        //
        // GET: /Customer/Details/5
        public ActionResult Details(int id)
        {
            Customer customer = repository.GetByID(id);
            if (customer == null)
            {
                TempData["message"] = "Customer not found!";
                return RedirectToAction("Index");
            }
           
            return View(customer);
        }

        //
        // GET: /Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Customer/Create
        [HttpPost]
        public ActionResult Create(Customer customer)
        {
            if (ModelState.IsValid)
            {               
                repository.Insert(customer);
                CommitTransactions.CommitAll();
                TempData["message"] = "Successfully created Customer";
                return RedirectToAction("Index");
            }
            return View();
        }

        //
        // GET: /Customer/Edit/5
        public ActionResult Edit(int id)
        {
            Customer customer = repository.GetByID(id);
            if (customer == null)
            {
                TempData["message"] = "Customer not found!";
                return RedirectToAction("Index");
            }

            return View(customer);
        }

        //
        // POST: /Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            Customer customer = repository.GetByID(id);
            if (customer == null)
            {
                TempData["message"] = "Customer not found!";
                return RedirectToAction("Index");
            }
            
            if (ModelState.IsValid)
            {
                UpdateModel(customer);
                repository.Update(customer);
                CommitTransactions.CommitAll();
                TempData["message"] = "Successfully updated Branch";
                return RedirectToAction("Index");
            }
            return View();
        }

        //
        // GET: /Customer/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
