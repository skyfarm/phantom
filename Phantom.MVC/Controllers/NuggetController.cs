﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phantom.Logic;
using Phantom.Core;
using Phantom.MVC.Models;

namespace Phantom.MVC.Controllers
{
    public class NuggetController : Controller
    {
        NuggetLogic logic = new NuggetLogic();
        //
        // GET: /Nugget/
        [ChildActionOnly]
        public ActionResult GetNugget()
        {
            Nugget nugget = logic.GetRandomNugget();      

            return PartialView("_Nugget",nugget);
        }
	}
}