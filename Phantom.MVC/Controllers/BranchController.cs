﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phantom.MVC.Models;
using Phantom.Logic;
using Phantom.Core;
namespace Phantom.MVC.Controllers
{
    public class BranchController : Controller
    {
        Repository<Branch> repository = new Repository<Branch>();
        //
        // GET: /Branch/
        public ActionResult Index()
        {
            List<Branch> branches = repository.GetAll();           
            return View(branches);
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Branch branch)
        {
            if (ModelState.IsValid)
            {
                repository.Insert(branch);
                CommitTransactions.CommitAll();
                TempData["message"] = "Successfully created Branch";             
                return RedirectToAction("Index");
            }
            return View();
        }

        //
        // GET: /Test/Edit/5
        public ActionResult Edit(int id)
        {
            Branch branch = repository.GetByID(id);
            //BranchModel test = branch as BranchModel;
            if (branch == null)
            {
                TempData["message"] = "Branch not found!";      
                return RedirectToAction("Index");
            }
    
            return View(branch);
        }
        // POST: /Test/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            Branch branch = repository.GetByID(id);
            if (branch == null)
            {
                TempData["message"] = "Branch not found!";
                return RedirectToAction("Index");
            }
            if (ModelState.IsValid)
            {
                UpdateModel(branch);
                repository.Update(branch);
                CommitTransactions.CommitAll();
                TempData["message"] = "Successfully updated Branch";
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult Details(int id)
        {
            Branch branch = repository.GetByID(id);
            if (branch == null)
            {
                TempData["message"] = "Branch not found!";
                return RedirectToAction("Index");
            }

            return View(branch);
        }

    }
}