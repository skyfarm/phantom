﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;

namespace Phantom.Data
{
    public class BranchDAO:RepositoryDAO<Branch>
    {
        public void InsertNewBranch(Branch branch)
        {
            Insert(branch);
        }

        public Branch GetBranchDetailsByID(int id)
        {
            return RetrieveByID(id);
        }
    }
}
