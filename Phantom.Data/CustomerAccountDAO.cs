﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using NHibernate.Linq;

namespace Phantom.Data
{
    public class CustomerAccountDAO : RepositoryDAO<CustomerAccount>
    {
        public void InsertNewCustomerAccount(CustomerAccount account)
        {
            Insert(account);
            if (account.AccountType == AccountType.Loan) 
            {
                 //Insert into Loan Table
            }
        }
       
      
        public List<CustomerAccount> RetrieveACustomersAccounts(Customer customer)
        {
            List<CustomerAccount> list = GetSession().Query<CustomerAccount>().Where(x => x.Customer.Id.Equals(customer.Id)).ToList();
            return list;
        }

        public List<CustomerAccount> RetrieveCustomerAccountsInABranch(Branch branch)
        {
            List<CustomerAccount> list = GetSession().Query<CustomerAccount>()
                .Where(x => x.Branch.Id.Equals(branch.Id))
                .Where(x => x.IsDisabled == false)
                .ToList();
            return list;
        }
        public List<CustomerAccount> RetreiveAllActiveCustomerAccounts()
        {
            List<CustomerAccount> list = GetSession().Query<CustomerAccount>()
                .Where(x => x.IsDisabled == false)
                .ToList();
            return list;
        }
        public List<CustomerAccount> RetreiveAllActiveCustomerAccountsOfType(AccountType type)
        {
            List<CustomerAccount> list = GetSession().Query<CustomerAccount>()
                .Where(x => x.IsDisabled == false)
                .Where(x => x.AccountType == type)
                .ToList();
            return list;
        }

        public CustomerAccount RetreiveByAccountNumber(string accountNumber)
        {
            CustomerAccount account;
            try
            {
                account = GetSession().QueryOver<CustomerAccount>()
                   .Where(x => x.AccountNumber == accountNumber)
                   .SingleOrDefault<CustomerAccount>();
                return account;
            }
            catch
            {
                return null;
            }
        }
        public List<CustomerAccount> RetrieveAllCustomerAccountsWithCards()
        {
            return GetSession().Query<CustomerAccount>()
                .Where(x => x.CardIssued == true)
                .ToList();            
        }

        public List<CustomerAccount> RetrieveAllCustomerAccountsWithoutCards()
        {
            return GetSession().Query<CustomerAccount>()
                .Where(x => x.CardIssued == false && x.AccountType != AccountType.Loan)
                .ToList();
        }
      
    }
}
