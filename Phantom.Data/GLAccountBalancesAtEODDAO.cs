﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using NHibernate.Linq;

namespace Phantom.Data
{
    public class GLAccountBalancesAtEODDAO : RepositoryDAO<GLAccountBalancesAtEOD>
    {
        public List<GLAccountBalancesAtEOD> RetreiveByCategory(AccountCategory category, DateTime date)
        {
            int sumID = 0;
            if (category == AccountCategory.Income)
            {
                sumID = 1017;
            }
            else if (category == AccountCategory.Expense)
            {
                sumID = 1018;
            }
            return GetSession().Query<GLAccountBalancesAtEOD>()
                .Where(x => x.GLAccount.Category.Category == category)
                .Where(x => x.GLAccount.Id != sumID)
                .Where(x => x.FinancialDate.Date == date.Date)
                .ToList();
        }
    }
}
