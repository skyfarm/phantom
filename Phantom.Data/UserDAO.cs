﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;

using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using NHibernate;
using NHibernate.Linq;

using NHibernate.Criterion;

namespace Phantom.Data
{
    public class UserDAO:RepositoryDAO<User>
    {
        public void InsertNewUser(User user)
        {
            //check if username already exists in the database   
        }
       
        public List<User> RetrieveAllNonTellers() 
        {
            List<User> list = GetSession().Query<User>().Where(x => x.IsTeller == false).ToList();
            return list;
        }
       
        public bool CheckExistingUser(string username, string email)
        {
            int count = GetSession().Query<User>()
                .Where(x => x.UserName == username)
                .Count();
            if(count > 0)
            {
                return true;
            }
            count = GetSession().Query<User>()
               .Where(x => x.Email == email)
               .Count();
            if(count > 0)
            {
                return true;
            }
            return false;
        }
        public bool Authenticate(string username, string password)
        {
            int count = GetSession().Query<User>()
                .Where(x => x.UserName == username)
                .Where(x => x.Password == password)
                .Count();

            if (count == 0)
            {
                return false;
            }
            else if (count == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        public User RetreiveUserByLogin(string username, string password)
        {
            List<User> list = GetSession().Query<User>()
                 .Where(x => x.UserName == username)
                 .Where(x => x.Password == password).ToList();

            return list.ElementAt(0);

        }
    }
}
