﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using Phantom.Data;
using NHibernate.Linq;


namespace Phantom.Data
{
    public class TellerDAO : RepositoryDAO<Teller>
    {
        public void InsertNewTeller(Teller teller)
        {
            Insert(teller);
        }
        //public List<Teller> RetrieveAllTellers() 
        //{
        //    return RetrieveList();
        //}
        public Teller RetrieveTellerByUserId(User user)
        {
            List<Teller> list =  GetSession().Query<Teller>().Where(x => x.User.Id.Equals(user.Id)).ToList();
            return list.First();
            //return list.ElementAt(0);

            
        }

    }
}
