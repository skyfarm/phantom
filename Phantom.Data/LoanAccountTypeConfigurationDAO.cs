﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;

namespace Phantom.Data
{
    public class LoanAccountTypeConfigurationDAO: RepositoryDAO<LoanAccountTypeConfiguration>
    {
        public void InsertNewLoanAccountTypeConfiguration(LoanAccountTypeConfiguration config)
        {
            Insert(config);
        }
        public void UpdateLoanAccountTypeConfiguration(LoanAccountTypeConfiguration config)
        {
            Update(config);
        }
        public LoanAccountTypeConfiguration RetreiveLoanAccountTypeConfiguration(int id)
        {
            return RetrieveByID(id);
        }
    }
}
