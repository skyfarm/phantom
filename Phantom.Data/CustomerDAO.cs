﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;

using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using NHibernate;
using NHibernate.Linq;

using NHibernate.Criterion;

namespace Phantom.Data
{
    public class CustomerDAO:RepositoryDAO<Customer>
    {
        public bool CheckExistingCustomer(string phoneNumber, string email)
        {
            int count = GetSession().Query<Customer>()
                .Where(x => x.Phone == phoneNumber)
                .Count();
            if (count > 0)
            {
                return true;
            }
            count = GetSession().Query<Customer>()
               .Where(x => x.Email == email)
               .Count();
            if (count > 0)
            {
                return true;
            }
            return false;
        }
    }
}
