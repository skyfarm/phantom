﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;

namespace Phantom.Data
{
    public class LoanDAO : RepositoryDAO<Loan>
    {
        public void InsertNewLoan(Loan loan) 
        {
            Insert(loan);
        }
    }
}
