﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;

namespace Phantom.Data
{
    public class CardDAO : RepositoryDAO<Card>
    {
        public bool CheckCard(string cardPAN, out Card card)
        {
        
            card = GetSession().QueryOver<Card>()
              .Where(x => x.CardPAN == cardPAN)
              .SingleOrDefault<Card>();

            if (card == null) 
            { 
                return false; 
            }
            return true;
           
        }
    }
}
