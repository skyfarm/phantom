﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;


namespace Phantom.Data
{
    public class NHibernateManager
    {
        private static ISessionFactory factory;
        private static ISession session;
        static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DBPhantom"].ConnectionString;

        public static void CreateDB()
        {
            Fluently.Configure()
               .Database(MsSqlConfiguration.MsSql2008.ConnectionString(connectionString))
               .Mappings(m => m.FluentMappings
               .AddFromAssemblyOf<Mappings.UserMap>())
               .ExposeConfiguration(CreateSchema)
               .BuildConfiguration();
        }
       
        private static void ResetSchema(NHibernate.Cfg.Configuration cfg)
        {
            cfg.SetProperty("current_session_context_class", "managed_web");
            new SchemaUpdate(cfg).Execute(false, true);
        }

        public static void ResetDB()
        {
            Fluently.Configure()
               .Database(MsSqlConfiguration.MsSql2008.ConnectionString(connectionString))
               .Mappings(m => m.FluentMappings
               .AddFromAssemblyOf<Mappings.UserMap>())
               .ExposeConfiguration(ResetSchema)
               .BuildConfiguration();
        }
        public static void CreateSchema(NHibernate.Cfg.Configuration cfg)
        {
            var schemaExport = new SchemaExport(cfg);
            schemaExport.Drop(false, true);
            schemaExport.Create(false, true);
        }

        private static ISessionFactory CreateSessionFactory()
        {
            return Fluently.Configure()
            .Database(MsSqlConfiguration.MsSql2008.ConnectionString(connectionString))
            .Mappings(m => m.FluentMappings
            .AddFromAssemblyOf<Mappings.UserMap>())
              .ExposeConfiguration(ResetSchema)
               .BuildConfiguration()
            .BuildSessionFactory();
        }
        public static ISession Session()
        {
            if (factory == null)
            {
                factory = CreateSessionFactory();
            }
            if (session == null)
            {
                session = factory.OpenSession();
            }

            session.BeginTransaction();

            return session;
        }
    }
}
