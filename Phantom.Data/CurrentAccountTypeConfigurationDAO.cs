﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;

namespace Phantom.Data
{
    public class CurrentAccountTypeConfigurationDAO : RepositoryDAO<CurrentAccountTypeConfiguration>
    {
        public void InsertNewCurrentAccountTypeConfiguration(CurrentAccountTypeConfiguration config)
        {
            Insert(config);
        }
        public void UpdateCurrentAccountTypeConfiguration(CurrentAccountTypeConfiguration config)
        {
            Update(config);
        }
        public CurrentAccountTypeConfiguration RetreiveCurrentAccountTypeConfiguration(int id)
        {
            return RetrieveByID(id);
        }
    }
}
