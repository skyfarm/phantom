﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using FluentNHibernate.Mapping;

namespace Phantom.Data.Mappings
{
    public class GLAccountMap:ClassMap<GLAccount>
    {
        public GLAccountMap()
        {
            Id(x => x.Id);
            Map(x => x.Code).Not.Nullable();
            Map(x => x.Name).Not.Nullable();
            Map(x => x.AccountBalance).Not.Nullable().Default("0");
            References(x => x.Category).Not.Nullable();
            References(x => x.Branch).Not.Nullable();
            Map(x => x.IsAssigned).Not.Nullable().Default("0");
            Map(x => x.DateCreated).Not.Nullable();
            Map(x => x.DateModified).Not.Nullable();
        }
    }
}
