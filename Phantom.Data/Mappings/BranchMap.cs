﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Phantom.Core;

namespace Phantom.Data.Mappings
{
    public class BranchMap:ClassMap<Branch>
    {
        public BranchMap()
        {
            Id(x => x.Id).Not.Nullable();
            Map(x => x.Code).Not.Nullable();
            Map(x => x.Name).Not.Nullable();
            Map(x => x.Status).CustomType<Status>().Not.Nullable();
            Map(x => x.DateCreated).Not.Nullable();
            Map(x => x.DateModified).Not.Nullable();
        }
    }
}
