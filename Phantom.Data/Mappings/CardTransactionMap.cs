﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Phantom.Core;

namespace Phantom.Data.Mappings
{
    public class CardTransactionMap : ClassMap<CardTransaction>
    {
        public CardTransactionMap()
        {
            Id(x => x.Id);
            Map(x => x.CardHolderName);
            Map(x => x.AccountNumber);
            Map(x => x.CardPan);
            Map(x => x.MTI);
            Map(x => x.STAN);
            Map(x => x.TransactionDate);
            Map(x => x.TransactionType);
            Map(x => x.Amount);
            Map(x => x.Fee);
            Map(x => x.ResponseCode);
            Map(x => x.ResponseDescription);
            Map(x => x.IsReversed);
            Map(x => x.OriginalDataElement);
            Map(x => x.DateCreated);
            Map(x => x.DateModified);
        }
    }
}
