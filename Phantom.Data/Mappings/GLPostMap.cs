﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Phantom.Core;

namespace Phantom.Data.Mappings
{
    public class GLPostMap : ClassMap<GLPost>
    {
        public GLPostMap()
        {
            Id(x => x.Id);
            References(x => x.GLAccountToDebit).Not.Nullable();
            Map(x => x.DebitAmount).Not.Nullable();
            References(x => x.GLAccountToCredit).Not.Nullable();
            Map(x => x.CreditAmount).Not.Nullable();
            Map(x => x.Narration).Not.Nullable();
            Map(x => x.DateCreated).Not.Nullable();
            Map(x => x.DateModified).Not.Nullable();
        }
    }
}
