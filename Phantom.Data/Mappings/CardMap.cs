﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Phantom.Core;

namespace Phantom.Data.Mappings
{
    public class CardMap : ClassMap<Card>
    {
        public CardMap()
        {
            Id(x => x.Id);
            References(x => x.LinkedAccount);
            Map(x => x.CardPAN);
            Map(x => x.CVV);
            Map(x => x.ExpiryDate);
            Map(x => x.DateCreated);
            Map(x => x.DateModified);
        }
    }
}
