﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using FluentNHibernate.Mapping;

namespace Phantom.Data.Mappings
{
    public class UserMap:ClassMap<User>
    {
        public UserMap()
        {
            Id(x => x.Id);
            Map(x => x.FirstName).Not.Nullable();
            Map(x => x.Surname).Not.Nullable();
            Map(x => x.Email).Not.Nullable();
            Map(x => x.Phone).Not.Nullable();
            Map(x => x.Gender).CustomType<Gender>().Not.Nullable();
            References(x => x.Branch).Not.Nullable();
            Map(x => x.UserName).Not.Nullable();
            Map(x => x.Password).Not.Nullable();
            Map(x => x.Level).CustomType<AdminLevel>().Not.Nullable();
           // Map(x => x.IsTeller).CustomType<IsTeller>();
            Map(x => x.IsTeller).Not.Nullable().Default("0");
           // Map(x => x.IsTeller).CustomType<IsTeller>().Not.Nullable();
            Map(x => x.DateCreated).Not.Nullable();
            Map(x => x.DateModified).Not.Nullable();
        }
    }
}
