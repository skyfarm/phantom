﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using FluentNHibernate.Mapping;

namespace Phantom.Data.Mappings
{
    public class GLAccountBalancesAtEODMap : ClassMap<GLAccountBalancesAtEOD>
    {
        public GLAccountBalancesAtEODMap()
        {
            Id(x => x.Id);
            References(x => x.GLAccount).Not.Nullable();
            Map(x => x.AccountBalanceAtEOD).Not.Nullable();
            Map(x => x.FinancialDate).Not.Nullable();
            Map(x => x.DateCreated).Not.Nullable();
            Map(x => x.DateModified).Not.Nullable();
        }
    }
}
