﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using FluentNHibernate.Mapping;

namespace Phantom.Data.Mappings
{
    public class CardPostMap : ClassMap<CardPost>
    {
        public CardPostMap()
        {
            Id(x => x.Id);
            References(x => x.CustomerAccount);
            Map(x => x.CardTransactionType).CustomType<CardTransactionType>();
            Map(x => x.Amount);
            Map(x => x.Fee);
            Map(x => x.Channel).CustomType<Channel>();
            References(x => x.ToAccount);
           // Map(x => x.Narration);
            Map(x => x.DateCreated);
            Map(x => x.DateModified);
        }
    }
}
