﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using FluentNHibernate.Mapping;

namespace Phantom.Data.Mappings
{
    public class CurrentAccountTypeConfigurationMap : ClassMap<CurrentAccountTypeConfiguration>
    {
        public CurrentAccountTypeConfigurationMap() 
        {
            Id(x => x.Id);
            Map(x => x.CreditInterestRate).Not.Nullable();
            Map(x => x.MinimumBalance).Not.Nullable();
            References(x => x.InterestExpenseGLAccount).Not.Nullable();
            Map(x => x.COT).Not.Nullable();
            References(x => x.COTIncomeGLAccount).Not.Nullable();
            Map(x => x.DateCreated).Not.Nullable();
            Map(x => x.DateModified).Not.Nullable();
        }
    }
}
