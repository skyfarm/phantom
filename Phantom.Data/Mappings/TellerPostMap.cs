﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using FluentNHibernate.Mapping;

namespace Phantom.Data.Mappings
{
    public class TellerPostMap : ClassMap<TellerPost>
    {
        public TellerPostMap()
        {
            Id(x => x.Id);
            References(x => x.CustomerAccount).Not.Nullable();
            Map(x => x.PostType).CustomType<PostingType>().Not.Nullable();
            Map(x => x.Amount).Not.Nullable();
            Map(x => x.Narration).Not.Nullable();
            References(x => x.Teller).Not.Nullable();
            Map(x => x.DateCreated).Not.Nullable();
            Map(x => x.DateModified).Not.Nullable();
        }
    }
}
