﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Phantom.Core;

namespace Phantom.Data.Mappings
{
    public class NuggetMap : ClassMap<Nugget>
    {
        public NuggetMap()
        {
            Id(x => x.Id);
            Map(x => x.Number);
            Map(x => x.Tip).Length(4001);
            Map(x => x.DateCreated);
            Map(x => x.DateModified);
        }
    }
}
