﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Phantom.Core;

namespace Phantom.Data.Mappings
{
    public class BusinessStatusMap  : ClassMap<BusinessStatus>
    {
        public BusinessStatusMap()
        {
            Id(x => x.Id);
            Map(x => x.Status).CustomType<StateOfBusiness>().Not.Nullable();
            Map(x => x.DateCreated).Not.Nullable();
            Map(x => x.DateModified).Not.Nullable();
        }
    }
}
