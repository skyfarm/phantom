﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using FluentNHibernate.Mapping;

namespace Phantom.Data.Mappings
{
    public class LoanAccountTypeConfigurationMap : ClassMap<LoanAccountTypeConfiguration>
    {
        public LoanAccountTypeConfigurationMap()
        {
            Id(x => x.Id);
            Map(x => x.DebitInterestRate).Not.Nullable();
            References(x => x.InterestIncomeGLAccount).Not.Nullable();
            Map(x => x.DateCreated).Not.Nullable();
            Map(x => x.DateModified).Not.Nullable();
        }
    }
}
