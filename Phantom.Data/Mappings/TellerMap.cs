﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Phantom.Core;

namespace Phantom.Data.Mappings
{
    public class TellerMap : ClassMap<Teller>
    {
        public TellerMap()
        {
            Id(x => x.Id);
            //Map(x => x.IsTeller).CustomType<IsTeller>().Not.Nullable();
            References(x => x.User).Not.Nullable();
            References(x => x.TillAccount).Not.Nullable();
            Map(x => x.DateCreated).Not.Nullable();
            Map(x => x.DateModified).Not.Nullable();
        }
    }
}
