﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Phantom.Core;

namespace Phantom.Data.Mappings
{
    public class CustomerAccountMap : ClassMap<CustomerAccount>
    {
        public CustomerAccountMap() 
        {
            Id(x => x.Id);
            References(x => x.Customer).Not.Nullable();
            Map(x => x.AccountName).Not.Nullable();
            Map(x => x.AccountNumber).Not.Nullable();
            References(x => x.Branch).Not.Nullable();
            Map(x => x.AccountType).CustomType<AccountType>().Not.Nullable();
            Map(x => x.AccountBalance).Not.Nullable().Default("0");
            Map(x => x.IsDisabled).Not.Nullable().Default("0");
            Map(x => x.CardIssued).Not.Nullable().Default("0");
            Map(x => x.DateCreated).Not.Nullable();
            Map(x => x.DateModified).Not.Nullable();
        }
    }
}
