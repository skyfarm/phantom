﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using FluentNHibernate.Mapping;

namespace Phantom.Data.Mappings
{
    public class LoanMap : ClassMap<Loan>
    {
        public LoanMap()
        {
            Id(x => x.Id);
            References(x => x.LinkedAccount).Not.Nullable();
            References(x => x.CustomerAccount).Not.Nullable();
            Map(x => x.Terms).Not.Nullable();
            Map(x => x.Amount).Not.Nullable();
            Map(x => x.DateCreated).Not.Nullable();
            Map(x => x.DateModified).Not.Nullable();
        }
    }
}
