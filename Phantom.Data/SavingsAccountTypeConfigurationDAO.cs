﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;

namespace Phantom.Data
{
    public class SavingsAccountTypeConfigurationDAO : RepositoryDAO<SavingsAccountTypeConfiguration>
    {
        public void InsertNewSavingsAccountTypeConfiguration(SavingsAccountTypeConfiguration config)
        {
            Insert(config);
        }
        public void UpdateSavingsAccountTypeConfiguration(SavingsAccountTypeConfiguration config)
        {
            Update(config);
        }
        public SavingsAccountTypeConfiguration RetreiveSavingsAccountTypeConfiguration(int id)
        {
            return RetrieveByID(id);
        }
    }
}
