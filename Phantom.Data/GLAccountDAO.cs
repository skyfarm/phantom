﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.Core;
using NHibernate.Linq;

namespace Phantom.Data
{
    public class GLAccountDAO : RepositoryDAO<GLAccount>
    {
        public void InsertNewGLAccount(GLAccount glAccount)
        {
            Insert(glAccount);
        }
        public void UpdateExistingGLAccount(GLAccount glAccount)
        {
            Update(glAccount);
        }

        public GLAccount RetreiveGLAccountDetails(int id)
        {
            return RetrieveByID(id);
        }
        public List<GLAccount> RetreiveAllGLAccounts()
        {
            return RetrieveList();
        }
        public List<GLAccount> RetreiveALLUnassignedGLAccounts() 
        {
            return GetSession().Query<GLAccount>().Where(x => x.IsAssigned == false)
                .Where(x => x.Category.Id == 1).ToList();//and branch_id = current user session.branch_id
        }
        public List<GLAccount> RetreiveALLUnassignedGLAccountsInABranch(Branch branch)
        {
            return GetSession().Query<GLAccount>().Where(x => x.IsAssigned == false)
                .Where(x => x.Category.Id == 1)
                .Where(x => x.Branch == branch).ToList();
                //and branch_id = current user session.branch_id
        }
    }
}
