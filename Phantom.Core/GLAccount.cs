﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Core
{
    public class GLAccount:Entity
    {
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual decimal AccountBalance { get; set; }
        public virtual GLCategory Category { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual bool IsAssigned { get; set; }
    }

}
