﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Core
{
    public class Nugget : Entity
    {
        public virtual string Number { get; set; }
        public virtual string Tip  { get; set; }
    }
}
