﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Core
{
    public class LoanAccountTypeConfiguration : Entity
    {
        public virtual decimal DebitInterestRate { get; set; }
        public virtual GLAccount InterestIncomeGLAccount { get; set; }
       
    }
}
