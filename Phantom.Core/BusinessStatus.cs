﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Core
{
    public class BusinessStatus: Entity
    {
        public virtual StateOfBusiness Status { get; set; }
    }
}
