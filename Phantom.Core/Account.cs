﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Core
{
    public class Account:Entity
    {
        public virtual string Name { get; set; }
        public virtual string AccountNumber { get; set; }
        public virtual decimal InterestRate { get; set; }
        public virtual decimal MinimumBalance { get; set; }
        public virtual GLAccount Interest { get; set; }//credit for savings and current, debit for loan

    }
}
