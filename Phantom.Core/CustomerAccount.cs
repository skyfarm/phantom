﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Core
{
    public class CustomerAccount:Entity
    {
        public virtual Customer Customer { get; set; }
        public virtual string AccountName { get; set; }
        public virtual string AccountNumber { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual AccountType AccountType { get; set; }
        public virtual decimal AccountBalance { get; set; }
        public virtual bool IsDisabled { get; set; }//probably should use isEnabled
        public virtual bool CardIssued { get; set; }
    }
}
