﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Core
{
    public class Card : Entity
    {
        public virtual CustomerAccount LinkedAccount { get; set; }
        public virtual string CardPAN { get; set; }
        public virtual string CVV { get; set; }
        public virtual DateTime ExpiryDate { get; set; }
    }
}
