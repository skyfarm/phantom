﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Core
{
    public class SavingsAccountTypeConfiguration : Entity
    {
        public virtual decimal CreditInterestRate { get; set; }
        public virtual decimal MinimumBalance { get; set; }
        public virtual GLAccount InterestExpenseGLAccount { get; set; }
    }
}
