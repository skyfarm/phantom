﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Core
{
    public class GLCategory : Entity
    {
        public virtual string Name { get; set; }
        public virtual AccountCategory Category { get; set; }
        public virtual string Description { get; set; }
    }
}
