﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Core
{
    public class CurrentAccount : Account
    {
        public virtual double COT { get; set; }
        //there is a GLAccount "Interest" in Account, so there is another here??
        public virtual GLAccount IncomeGL { get; set; }
    }
}
