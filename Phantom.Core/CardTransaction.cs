﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Core
{
    public class CardTransaction : Entity
    {
        public virtual string CardHolderName { get; set; }
        public virtual string AccountNumber { get; set; }
        public virtual string CardPan { set; get; }
        public virtual string MTI { set; get; }
        public virtual string STAN { set; get; }
        public virtual DateTime TransactionDate { set; get; }
        public virtual string TransactionType { set; get; }
        public virtual decimal Amount { set; get; }
        public virtual decimal Fee { get; set; }
        public virtual string ResponseCode { set; get; }
        public virtual string ResponseDescription { set; get; }
        public virtual bool IsReversed { get; set; }
        public virtual string OriginalDataElement { get; set; }
    }
}
