﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Core
{
    public class User:Person
    {
        public virtual string UserName { get; set; }
        public virtual string Password { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual AdminLevel Level { get; set; }
        //public virtual IsTeller IsTeller { get; set; }
        public virtual bool IsTeller { get; set; }
        
    }
}
