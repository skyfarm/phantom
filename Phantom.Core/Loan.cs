﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Core
{
    public class Loan:Entity
    {
        public virtual CustomerAccount LinkedAccount { get; set; }//savings or current account in the customeraccount table
        public virtual CustomerAccount CustomerAccount { get; set; }//loan account in the customeraccoutn table
        public virtual string Terms { get; set; }
        public virtual decimal Amount { get; set; }
    }
}
