﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Core
{
     public enum Status
     {
        Inactive = 0,
        Active = 1
     }
     public enum Gender
     {
         Male = 0,
         Female = 1
     }
     public enum AccountType
     {
         Savings = 0,
         Current = 1,
         Loan = 2
     }
     public enum AdminLevel
     {
         SuperUser = 0,
         BranchSuperUser = 1,
         //TellerUser = 2,
         RegularUser = 2
     }
     public enum AccountCategory
     {
         Asset = 1,
         Liability = 2,
         Capital = 3,
         Income = 4,
         Expense = 5
     }
     public enum IsTeller
     {
         No = 0,
         Yes = 1
     }
     public enum PostingType
     {
         Deposit = 0,
         Withdraw = 1
     }
     public enum StateOfBusiness
     {
         Closed = 0,
         Open = 1
     }
     public enum CardTransactionType
     {
         BalanceInquiry,
         Withdrawal,
         Transfer
     }
     public enum Channel
     {
         ATM,
         POS,
         Web,
         Unknown
     }

}