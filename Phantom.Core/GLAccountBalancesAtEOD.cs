﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Core
{
    public class GLAccountBalancesAtEOD : Entity
    {
        public virtual GLAccount GLAccount { get; set; }
        public virtual decimal AccountBalanceAtEOD { get; set; }
        public virtual DateTime FinancialDate { get; set; }
    }
}
