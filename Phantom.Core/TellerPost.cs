﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Core
{
    public class TellerPost : Entity
    {
        public virtual CustomerAccount CustomerAccount { get; set; }
        public virtual PostingType PostType { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual string Narration { get; set; }
        public virtual Teller Teller { get; set; }
    }
}
