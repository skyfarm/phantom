﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Core
{
    public class GLPost : Entity
    {
        public virtual GLAccount GLAccountToDebit { get; set; }
        public virtual decimal DebitAmount { get; set; }
        public virtual string Narration { get; set; }
        public virtual GLAccount GLAccountToCredit { get; set; }
        public virtual decimal CreditAmount { get; set; }

    }
}
