﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Core
{
    public class CardPost : Entity
    {
        public virtual CustomerAccount CustomerAccount { get; set; }
        public virtual CardTransactionType CardTransactionType { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual decimal Fee { get; set; }
        public virtual Channel Channel { get; set; }
        public virtual CustomerAccount ToAccount { get; set; }
       // public virtual string Narration { get; set; }
    }
}
