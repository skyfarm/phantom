﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.Core
{
    public class Branch:Entity
    {
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual Status Status { get; set; }

    }
}
