﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phantom.FEP.ISO_Message_Codes
{
    public class ISOMessageCodeUtility
    {
        public static string GetResponseCodeDescription(string responseCode)
        {
            string responseCodeDescription = string.Empty;
            if (ISOMessageCodes.ResponseCodes.ContainsKey(responseCode))
            {
                responseCodeDescription = ISOMessageCodes.ResponseCodes.First(x => x.Key == responseCode).Value;
                return responseCodeDescription;
            }
            else
            {
                return responseCodeDescription = "Invalid Response Code";
            }
        }
    }
}
