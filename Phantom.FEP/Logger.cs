﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Phantom.FEP
{
    public class Logger
    {
        public void Log(string message)
        {
            System.Diagnostics.Trace.TraceWarning(message);
            //using (StreamWriter LogWriter = new StreamWriter(@"C:\Users\Seun\Documents\Visual Studio 2013\Projects\Phantom\Phantom.FEP Logs\Logs.txt", true))
            //{
            //    LogWriter.WriteLine(message + " " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt"));
            //    LogWriter.Close();
            //}

            Console.WriteLine("\n " + message + " " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt"));
        }
    }
}
