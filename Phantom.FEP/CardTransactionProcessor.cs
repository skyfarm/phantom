﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trx.Messaging.FlowControl;
using Trx.Messaging.Channels;
using Trx.Messaging.Iso8583;
using Phantom.Core;
using Phantom.Logic;
using Phantom.FEP.ISO_Message_Codes;

namespace Phantom.FEP
{
    public class CardTransactionProcessor
    {
        static Logger logger = new Logger();
        Channel channel; //this guy is declared here so he could be used throgh out scope.
        public Iso8583Message ProcessCardTransaction(Iso8583Message message)
        {
            if (ValidateCard(message) == false)
            {
                return SetResponseMessage(message, "14");
            }
            string channelCode = message.Fields[41].ToString().Substring(0, 1);
            channel = GetChannelByCode(channelCode);
            string transactionTypeCode = message.Fields[3].Value.ToString().Substring(0, 2);
            switch (transactionTypeCode)
            {
                case "31":
                    message = DoBalanceInquiry(message);
                    break;
                case "01":
                    message = DoWithdrawal(message);
                    break;
                case "50":
                    message = DoTransfer(message);
                    break;
                default:
                    message = null;
                    break;
            }

            return message;
        }

        public Iso8583Message ProcessReversal(Iso8583Message message)
        {
            throw new NotImplementedException();
        }

        private bool ValidateCard(Iso8583Message message)
        {
            logger.Log("Validating card...");
            string cardPAN = message.Fields[2].Value.ToString();
            string accountNumber = message.Fields[102].Value.ToString();

            Card card;
            if (CheckIfCardExists(cardPAN, out card) == false)
            {
                logger.Log("Invalid Card");
                return false;
            }
            if (ConfirmAccountWithCardPAN(card, accountNumber) == false)
            {
                logger.Log("Invalid Card");
                return false;
            }
            logger.Log("Valid Card");
            return true;
        }

        private Iso8583Message DoBalanceInquiry(Iso8583Message message)
        {
            logger.Log("This is a Balance Inquiry Transaction");
            CustomerAccount customerAccount = new CustomerAccountLogic().GetByAccountNumber(message.Fields[102].Value.ToString());
            decimal accountBalance = customerAccount.AccountBalance;
            string balance = Convert.ToInt32(SetAmountToISOAmountFormat(accountBalance)).ToString();
            message.Fields.Add(54, balance);
            //New Post
            
            logger.Log("Balance Inquiry Successful");
            NewCardPost(customerAccount, CardTransactionType.BalanceInquiry, channel);
            return SetResponseMessage(message,"00");
        }

        private Iso8583Message DoWithdrawal(Iso8583Message message)
        {
            logger.Log("This is a Withdrawal Transaction");
            CustomerAccount account = new CustomerAccountLogic().GetByAccountNumber(message.Fields[102].Value.ToString());
            decimal amount = GetAmountFromISOAmountFormat(Convert.ToDecimal(message.Fields[4].Value.ToString()));
            decimal fee;
            try
            {
                fee = GetFeeFromISOFeeFormat(message.Fields[28].Value.ToString());
            }
            catch (Exception)
            {
                fee = 0;
            }

            decimal totalAmount = amount + fee;
            if (!new CardPostLogic().IsFundsSufficientInCustomerAccount(account, totalAmount)) 
            {
                logger.Log("Insufficient Funds in Customer Account : "+ account.AccountName);
                return SetResponseMessage(message, "51");
            }
            
            if (!new CardPostLogic().IsFundsSufficientInTillAccount(amount))
            {
                logger.Log("Insufficient Funds in Till");
                return SetResponseMessage(message, "51");
            }

            //MAIN PART!!!
            new CardPostLogic().CashWithdrawalPost(account, amount, fee);

            logger.Log("Withdrawal Successful");

            //New Post
            NewCardPost(account, CardTransactionType.Withdrawal, channel, amount, fee);
            
            return SetResponseMessage(message, "00");
            
        }

        private Iso8583Message DoTransfer(Iso8583Message message)
        {
            logger.Log("This is a Payment/Transfer Transaction");
            CustomerAccount fromAccount = new CustomerAccountLogic().GetByAccountNumber(message.Fields[102].Value.ToString());
            CustomerAccount toAccount = new CustomerAccountLogic().GetByAccountNumber(message.Fields[103].Value.ToString());
            if (toAccount == null)
            {
                return SetResponseMessage(message, "39");
            }
            //try
            //{
            //     toAccount = new CustomerAccountLogic().GetByAccountNumber(message.Fields[103].Value.ToString());
            //}
            //catch (Exception)
            //{
            //    //destination account number is wrong
            //    return SetResponseMessage(message, "39");
            //}
            decimal amount = GetAmountFromISOAmountFormat(Convert.ToDecimal(message.Fields[4].Value.ToString()));
            decimal fee;
            try
            {
                fee = GetFeeFromISOFeeFormat(message.Fields[28].Value.ToString());
            }
            catch (Exception)
            {
                fee = 0;
            }

            decimal totalAmount = amount + fee;
            if (!new CardPostLogic().IsFundsSufficientInCustomerAccount(fromAccount, totalAmount))
            {
                logger.Log("Insufficient Funds in Customer Account : " + fromAccount.AccountName);
                return SetResponseMessage(message, "51");
            }
           
            if (!new CardPostLogic().IsFundsSufficientInTillAccount(totalAmount))
            {
                logger.Log("Insufficient Funds in Till");
                return SetResponseMessage(message, "51");
            }

           
            new CardPostLogic().FundsTransferPost(fromAccount, toAccount, amount, fee);

            logger.Log("Funds Transfer Successful");

            NewCardPost(fromAccount, CardTransactionType.Transfer, channel, amount, fee, toAccount);

            return SetResponseMessage(message, "00");
        }

        private bool CheckIfCardExists(string cardPAN, out Card card)
        {
            return new CardLogic().CheckIfCardExist(cardPAN, out card);
        }

        private bool ConfirmAccountWithCardPAN(Card card, string accountNumber)
        {
            if (card.LinkedAccount.AccountNumber == accountNumber)
            {
                return true;
            }
            return false;
        }

        private Iso8583Message SetResponseMessage(Iso8583Message message, string reponseCode)
        {
            message.SetResponseMessageTypeIdentifier();
            message.Fields.Add(39, reponseCode);
            return message;
        } //done

        private decimal GetAmountFromISOAmountFormat(decimal amountInMinorDenomination)
        {
            return (amountInMinorDenomination / 100);//convert kobo to naira
        }

        private decimal SetAmountToISOAmountFormat(decimal rawAmount)
        {
            return (rawAmount * 100);//convert naira to kobo
        }

        private decimal GetFeeFromISOFeeFormat(string fee)
        {
            string feeString = fee.Remove(0, 1); //remove C or D in front
           
            //string fee  = feeFieldValue.TrimStart('0').Length > 0 ? feeFieldValue.TrimStart('0') : "0";

            return GetAmountFromISOAmountFormat(Convert.ToDecimal(feeString));

        }

        private string GetTransactionTypeDescription(string transactionTypeCode)
        {
            string transactionTypeDescription = string.Empty;
            switch (transactionTypeCode)
            {
                case "31":
                    transactionTypeDescription = "Balance Enquiry";
                    break;
                case "01":
                    transactionTypeDescription = "Withdrawal";
                    break;
                case "50":
                    transactionTypeDescription = "Transfer/Payment";
                    break;
                default:
                    transactionTypeDescription = "";
                    break;
            }
            return transactionTypeDescription;
        }

        private Channel GetChannelByCode(string channelCode)
        {
            switch (channelCode)
            {
                case "1":
                    return Channel.ATM;
                case "2":
                    return Channel.POS;
                case "3":
                    return Channel.Web;
                default:
                    return Channel.Unknown;
            }
        }

        public void LogTransaction(Iso8583Message message)
        {
            string mti = message.MessageTypeIdentifier.ToString();

            string responseCode = string.Empty;

            
            try 
            {
                responseCode = message.Fields[39].Value.ToString(); 
            }
            catch { }

            decimal fee = 0;
            try 
            {
                fee = Convert.ToDecimal(message.Fields[28].ToString()); 
            }
            catch { }
            //string responseCode = mti == "210" || mti == "430" ? IsoMessage.Fields[39].Value.ToString() : null;
            //string responseCode = !string.IsNullOrWhiteSpace(IsoMessage.Fields[39].Value.ToString()) ? 
            //                                              IsoMessage.Fields[39].Value.ToString() : string.Empty;
            string originalDataElement = message.Fields[90].ToString();
           
            
            CustomerAccount customerAccount = new CustomerAccountLogic().GetByAccountNumber(message.Fields[102].ToString());
            if (customerAccount == null)
            {
                //no account matches the account number specified.
                //return something
            }
            CardTransaction cardTransaction = new CardTransaction() 
            {  
                AccountNumber = customerAccount != null ? customerAccount.AccountNumber : string.Empty,
                Amount = GetAmountFromISOAmountFormat(Convert.ToDecimal(message.Fields[4].Value)),
                CardHolderName = customerAccount!= null ? customerAccount.Customer.FirstName+ " "+customerAccount.Customer.Surname : string.Empty,
                CardPan = message.Fields[2].Value.ToString(),
                Fee = fee,
                IsReversed = mti == "430" && responseCode == "00" ? true : false,
                MTI = mti,
                OriginalDataElement =  originalDataElement.Length > 19 ? originalDataElement.Remove(0, (originalDataElement.Length - 19)) : originalDataElement,
                ResponseCode = responseCode,
                ResponseDescription = !string.IsNullOrWhiteSpace(responseCode)? ISO_Message_Codes.ISOMessageCodeUtility.GetResponseCodeDescription(responseCode) : string.Empty,
                STAN = message.Fields[11].Value.ToString(),
                TransactionDate = DateTime.Now,
                TransactionType = GetTransactionTypeDescription(message.Fields[3].Value.ToString().Substring(0,2))
                
            };

            new CardTransactionLogic().Insert(cardTransaction);

            logger.Log("Logged Transaction to Database");
            CommitTransactions.CommitAll();
        }

        private void NewCardPost(CustomerAccount account, CardTransactionType transactionType, Channel channel, 
            decimal amount = 0, decimal fee = 0, CustomerAccount toAccount = null)
        {
            CardPost cardPost = new CardPost() 
            { 
                CustomerAccount = account,
                CardTransactionType = transactionType,
                Channel = channel,
                Amount = amount,
                Fee = fee,
                ToAccount = toAccount
            };

            new CardPostLogic().Insert(cardPost);
            logger.Log("New Post Inserted");
            CommitTransactions.CommitAll();
        }
    }
}
