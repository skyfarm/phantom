﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trx.Messaging.Iso8583;
using Trx.Messaging.Channels;
using Trx.Messaging.FlowControl;


namespace Phantom.FEP.Peer_Connection
{
    public class Listener
    {
        static Logger logger = new Logger();

        private string localInterface = string.Empty; //IPAdress
        private int port = 0; //port
        public Listener()
        {
            try
            {
                //Check App.config for ip and port
                localInterface = System.Configuration.ConfigurationManager.AppSettings["AcquirerHostName"];
                port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["AcquirerPort"]);
            }
            catch
            {
                //if not found, set to this
                localInterface = "127.0.0.1";
                port = 2080;
            }
        }

        public void StartListener()
        {
            TcpListener tcpListener = new TcpListener(port);
            tcpListener.LocalInterface = localInterface;
            tcpListener.Start();

            ListenerPeer listenerPeer  = new ListenerPeer("Phoenix", new TwoBytesNboHeaderChannel(
                new Iso8583Ascii1987BinaryBitmapMessageFormatter()), tcpListener);

            logger.Log("Phantom FEP now listening at "+localInterface+" on "+port);

            listenerPeer.Connected += new PeerConnectedEventHandler(ListenerPeerConnected);
            listenerPeer.Receive += new PeerReceiveEventHandler(ListenerPeerReceive);
            listenerPeer.Disconnected += new PeerDisconnectedEventHandler(ListenerPeerDisconnected);

            listenerPeer.Connect();
        }

        private void ListenerPeerReceive(object sender, ReceiveEventArgs e)
        {
            ListenerPeer listenerPeer = sender as ListenerPeer;
            Iso8583Message message = e.Message as Iso8583Message;

            if (message == null) { return; }

            logger.Log("Receiving Message >>>>");
            CardTransactionProcessor processor = new CardTransactionProcessor();
            Iso8583Message response;
            try
            {
                processor.LogTransaction(message);

                if (message.IsReversalOrChargeBack())
                {
                    response = processor.ProcessReversal(message);
                }
                else
                {
                    response = processor.ProcessCardTransaction(message);
                }

                processor.LogTransaction(response);
                logger.Log("Sending Response >>>>");
            }
            catch (Exception)
            {
                message.Fields.Add(39, "06");
                response = message;

                processor.LogTransaction(response);
                logger.Log("Error, Something went wrong somewhere");
            }

            listenerPeer.Send(response);
            listenerPeer.Close();
            listenerPeer.Dispose();
            
        }
        private void ListenerPeerConnected(object sender, EventArgs e)
        {
            ListenerPeer listenerPeer = sender as ListenerPeer;
            if (listenerPeer == null) return;
            logger.Log("Listener Peer connected to " + listenerPeer.Name);
        }
        private void ListenerPeerDisconnected(object sender, EventArgs e)
        {
            ListenerPeer listenerPeer = sender as ListenerPeer;
            if (listenerPeer == null) return;
            logger.Log("Listener Peer disconnected from " + listenerPeer.Name);
   
            StartListener();
        }
    }
}
