﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phantom.FEP.Peer_Connection;
namespace Phantom.FEP
{
    class Program
    {
        static Logger logger = new Logger();
        static void Main(string[] args)
        {
            Listener connection = new Listener();
            connection.StartListener();
            try
            {
                
                while (true)
                {
                    
                    Console.WriteLine("Phantom FEP waiting for connection >>>");
                    Console.WriteLine();
                   
                    Console.Write("Type 'exit' to exit ");
                    Console.WriteLine();
                    string userResponse = Console.ReadLine();

                    if (userResponse == "exit")
                    {
           
                        Environment.Exit(0);
                    }
                    else
                    {
                        Console.WriteLine("Wrong input, try again");
                    }
                }
             
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Console.WriteLine(ex.Source);
                Console.WriteLine(ex.StackTrace);
                logger.Log("Error: " + ex.Message);
            }
        }
    }
}
