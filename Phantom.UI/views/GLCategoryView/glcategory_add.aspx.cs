﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.GLCategoryView
{
    public partial class glcategory_add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlCategory.DataSource = Enum.GetValues(typeof(AccountCategory));
                ddlCategory.DataBind();
                ddlCategory.Items.Insert(0, new ListItem("Select Category", "", true));
            }

        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            
            GLCategory glCategory = new GLCategory()
            {
                Name = txtName.Text,
                Category = (AccountCategory)Enum.Parse(typeof(AccountCategory), ddlCategory.Value),
                Description = txtDescription.Text
            };

            GLCategoryLogic glcLogic = new GLCategoryLogic();
            glcLogic.Insert(glCategory);
            CommitTransactions.CommitAll();

            Session["msg"] = "General Ledger Category Successfully  Added";

            Response.Redirect("glcategory_list.aspx", true);
        }
    }
}