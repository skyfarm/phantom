﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.GLCategoryView
{
    public partial class glcategory_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["id"] != null)
                {
                    int id = Convert.ToInt32(Session["id"].ToString());
                    GLCategoryLogic uLogic = new GLCategoryLogic();
                    GLCategory glCategory = uLogic.GetByID(id);

                    txtName.Text = glCategory.Name.ToString();

                    ddlCategory.DataSource = Enum.GetValues(typeof(AccountCategory));
                    ddlCategory.DataBind();
                    ddlCategory.Items.FindByValue(glCategory.Category.ToString()).Selected = true;

                    txtDescription.Text = glCategory.Description.ToString();
                }
                else
                {
                    Response.Redirect("../Home/index.aspx");
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Session["id"].ToString());
            GLCategoryLogic glcLogic = new GLCategoryLogic();
            GLCategory glCategory = glcLogic.GetByID(id);

            glCategory.Name = txtName.Text;
            glCategory.Category = (AccountCategory)Enum.Parse(typeof(AccountCategory), ddlCategory.Value);
            glCategory.Description = txtDescription.Text;

            

            glcLogic.Update(glCategory);
            CommitTransactions.CommitAll();

            Session["msg"] = "General Ledger Category Information Updated";

            Response.Redirect("glcategory_list.aspx",true);
           
        }
    }
}