﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.UserView
{
    public partial class user_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["id"] != null)
                {
                    int id = Convert.ToInt32(Session["id"].ToString());
                    UserLogic uLogic = new UserLogic();
                    User user = uLogic.GetByID(id);

                    BranchLogic bLogic = new BranchLogic();
                    List<Branch> listOfBranches = bLogic.GetAll();


                    txtFirstname.Text = user.FirstName.ToString();
                    txtSurname.Text = user.Surname.ToString();
                    txtEmail.Text = user.Email.ToString();
                    txtPhone.Text = user.Phone.ToString();

                    ddlGender.DataSource = Enum.GetValues(typeof(Gender));
                    ddlGender.DataBind();
                    ddlGender.Items.FindByValue(user.Gender.ToString()).Selected = true;

                    txtUsername.Text = user.UserName.ToString();

                    ddlLevel.DataSource = Enum.GetValues(typeof(AdminLevel));
                    ddlLevel.DataBind();
                    ddlLevel.Items.FindByValue(user.Level.ToString()).Selected = true;

                    ddlBranch.DataSource = listOfBranches;
                    ddlBranch.DataTextField = "Name";
                    ddlBranch.DataValueField = "Id";
                    ddlBranch.DataBind();
                    ddlBranch.Items.FindByText(user.Branch.Name.ToString()).Selected = true;
                    //ddlBranch.Items.FindByText( listOfBranches.Where(x=>x.Id == user.Branch.Id).FirstOrDefault().Name).Selected = true;
                    
                   
                }
                else
                {
                    Response.Redirect("../Home/index.aspx");
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Session["id"].ToString());
            UserLogic uLogic = new UserLogic();
            User user = uLogic.GetByID(id);

            BranchLogic bLogic = new BranchLogic();

            user.FirstName = txtFirstname.Text;
            user.Surname = txtSurname.Text;
            user.Email = txtEmail.Text;
            user.Phone = txtPhone.Text;
            user.Gender = (Gender)Enum.Parse(typeof(Gender), ddlGender.Value);
            user.UserName = txtUsername.Text;
            user.Level = (AdminLevel)Enum.Parse(typeof(AdminLevel),ddlLevel.Value);
            user.Branch = bLogic.GetByID(Convert.ToInt32(ddlBranch.Value));

            uLogic.Update(user);
            CommitTransactions.CommitAll();

            Session["msg"] = "User Information Updated";

            Response.Redirect("user_list.aspx",true);
           
        }
    }
}