﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.UserView
{
    public partial class user_list : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["msg"] != null)
                {
                    alertBox.Visible = true;
                    //alertBox.InnerHtml = Session["msg"].ToString();
                    message.InnerHtml = Session["msg"].ToString();
                }
                if(Session["err"] != null)
                {
                    message.InnerHtml = Session["err"].ToString();
                }
                UserLogic uLogic = new UserLogic();
                List<User> list = uLogic.GetAll();
                
                dataTable.DataSource = list;
                dataTable.DataBind();
            }
        }

        protected void dataTable_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditRow")
            {
                Session["Id"] = e.CommandArgument.ToString();
                Response.Redirect("user_edit.aspx");
            }
        }
    }
}