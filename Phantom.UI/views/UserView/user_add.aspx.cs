﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.UserView
{
    public partial class user_add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlGender.DataSource = Enum.GetValues(typeof(Gender));
                ddlGender.DataBind();
                //ddlGender.Items.Remove("Loan");
                ddlGender.Items.Insert(0, new ListItem("Select Gender", "", true));

                ddlLevel.DataSource = Enum.GetValues(typeof(AdminLevel));
                ddlLevel.DataBind();
                ddlLevel.Items.Insert(0, new ListItem("Select Level", "", true));

                BranchLogic bLogic = new BranchLogic();
                List<Branch> listOfBranches = bLogic.GetAll();
                ddlBranch.DataSource = listOfBranches;
                ddlBranch.DataTextField = "Name";
                ddlBranch.DataValueField = "Id";
                ddlBranch.DataBind();
                ddlBranch.Items.Insert(0, new ListItem("Select a Branch", "", true));

            }

        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            BranchLogic bLogic = new BranchLogic();
            User user = new User()
            {
                FirstName = txtFirstname.Text,
                Surname = txtSurname.Text,
                Email = txtEmail.Text,
                Phone = txtPhone.Text,
                Gender = (Gender)Enum.Parse(typeof(Gender),ddlGender.Value),
                UserName = txtUsername.Text,
                Level = (AdminLevel)Enum.Parse(typeof(AdminLevel),ddlLevel.Value),
                Branch = bLogic.GetByID(Convert.ToInt32(ddlBranch.Value))
            };

            UserLogic uLogic = new UserLogic();
            if(uLogic.CheckIfUsernameExists(user.UserName, user.Email) == true)
            {
                //username alreeady exists
                Session["err"] = "User Already Exists, Try differnt Username and/or  email";

                Response.Redirect("user_list.aspx", true);
            }
            else
            {
                uLogic.GeneratePasswordAndInsert(user);
                CommitTransactions.CommitAll();

                Session["msg"] = "User Successfully  Added";

                Response.Redirect("user_list.aspx", true);
            }
            
        }
    }
}