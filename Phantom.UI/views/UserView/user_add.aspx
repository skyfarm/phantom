﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Phantom.Master" AutoEventWireup="true" CodeBehind="user_add.aspx.cs" Inherits="Phantom.UI.views.UserView.user_add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHeader" runat="server">
    <h1>User Management</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
     <div class="row">
           <div class="col-md-6 col-md-offset-3">
               <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Add New User</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                   
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>First Name</label> 
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"  ControlToValidate="txtFirstname" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <asp:TextBox CssClass="form-control" ID="txtFirstname" runat="server" />
                                        </div>
                                        <div class="form-group">
                                            <label>Surname</label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2"  ControlToValidate="txtSurname" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                             <asp:TextBox CssClass="form-control" ID="txtSurname" runat="server" />
                                        </div>

                                        <div class="form-group">
                                            <label>Email address</label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3"  ControlToValidate="txtEmail" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="RegularExpressionValidator" ControlToValidate="txtEmail" ForeColor="Red" Text="Invalid Email Format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ></asp:RegularExpressionValidator>
                                            <asp:TextBox TextMode="Email" CssClass="form-control" ID="txtEmail" runat="server" />
                                        </div>

                                         <div class="form-group">
                                            <label>Phone</label>
                                              <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="RegularExpressionValidator" ControlToValidate="txtPhone" ForeColor="Red" Text="Invalid Phone Number Format, 8 or 11 numbers" ValidationExpression="[0-9]{11}|[0-9]{8}" ></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4"  ControlToValidate="txtEmail" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                             <asp:TextBox TextMode="Phone" CssClass="form-control" ID="txtPhone" runat="server" />
                                        </div>
                                        
    
                                        <!-- select -->
                                        <div class="form-group">
                                            <label>Gender</label>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator5"  ControlToValidate="txtEmail" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <select class="form-control" id="ddlGender" name="ddlGender" runat="server">                                                
                                            </select>
                                        </div>

                                     <div class="form-group">
                                            <label>Username</label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6"  ControlToValidate="txtUsername" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <%--<input type="text" class="form-control" placeholder="Enter ..." id="txtSurname" name="txtSurname" runat="server" />--%>
                                             <asp:TextBox CssClass="form-control" ID="txtUsername" runat="server" />
                                        </div>
                                   
                                     <div class="form-group">
                                            <label>Level</label>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator7"  ControlToValidate="ddlLevel" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <select class="form-control" id="ddlLevel" name="ddlLevel" runat="server">
                                                
                                            </select>
                                        </div>

                                     <div class="form-group">
                                            <label>Branch</label>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator8"  ControlToValidate="ddlBranch" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <select class="form-control" id="ddlBranch" name="ddlBranch" runat="server">
                                                
                                            </select>
                                        </div>
                                   
                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    <asp:Button type="submit" class="btn btn-primary" id ="btnInsert" name="btnInsert" runat="server" Text="Add" OnClick="btnInsert_Click"/>
                                    <%--<button type="submit" class="btn btn-primary" id ="btnUpdate" name="btnUpdate" runat="server">Submit</button>--%>
                                </div>
                            </div>       
              
            </div>                    
    </div>
</asp:Content>
