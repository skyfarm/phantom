﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.LoginView
{
    public partial class login : System.Web.UI.Page
    {
        public static User loggedInUser { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["err"] != null)
                {
                    alertBox.Visible = true;
                    message.InnerHtml = Session["err"].ToString();
                }
            }

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Value;
            string password = txtPassword.Value;

            

            UserLogic uLogic = new UserLogic();
            //List<User> list = uLogic.GetAll();

            if (uLogic.Authenticate(username, password) == true )
            {
                //loggedInUser = uLogic.GetUserByLogInDetails(username, password);
                User user = uLogic.GetUserByLogInDetails(username, password);
                Session["user"] = user;
                Response.Redirect("../Home/index.aspx");
            }
            else
            {
                //redirect
                Session["err"] = "Login attempt failed!";
                Response.Redirect("login.aspx");
            }
            //check with DB
        }
    }
}