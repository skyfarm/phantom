﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Phantom.UI.views.LoginView.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" class="bg-light-blue">
<head runat="server">
     <meta charset="UTF-8">
        <title>Phantom CBA</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../../css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
</head>
<body class="bg-light-blue">
    <form id="form1" runat="server">
        
         <div class="form-box" id="login-box">
            

             <div class="alert alert-danger alert-dismissable" id="alertBox" runat="server" visible="false">
                 <i class="fa fa-ban"></i>
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                 <b>Alert!</b><p id="message" runat="server">Lorem Ipsum</p>
              </div>

            <div class="header">Phantom</div>
            
                <div class="body bg-gray">
                    <div class="form-group">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8"  ControlToValidate="txtUsername" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                        <input type="text" id="txtUsername" name="txtUsername" class="form-control" placeholder="Username" runat="server"/>
                    </div>
                    <div class="form-group">
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator1"  ControlToValidate="txtPassword" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                        <input type="password" id="txtPassword" name="txtPassword" class="form-control" placeholder="Password" runat="server"/>
                    </div>          
                   
                </div>
                <div class="footer">
                    <asp:Button type="submit" class="btn bg-blue btn-block" Text="Sign Me In!" runat ="server" ID="btnLogin" name="btnLogin" OnClick="btnLogin_Click" />                                                               
                    
                   
                    
                </div>
            
        </div>
    </form>
</body>
</html>
