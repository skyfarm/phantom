﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.AccountTypeConfigView
{
    public partial class currentaccountconfig : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["msg"] != null)
                {
                    alertBox.Visible = true;
                    //alertBox.InnerHtml = Session["msg"].ToString();
                    message.InnerHtml = Session["msg"].ToString();
                }
                CurrentAccountTypeConfigurationLogic catcLogic = new CurrentAccountTypeConfigurationLogic();
                List<CurrentAccountTypeConfiguration> list = catcLogic.GetAll();
               
                
                dataTable.DataSource = list;
                dataTable.DataBind();
            }
        }

        protected void dataTable_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditRow")
            {
                Session["Id"] = e.CommandArgument.ToString();
                Response.Redirect("currentaccountconfig_edit.aspx");
            }
        }
    }
}