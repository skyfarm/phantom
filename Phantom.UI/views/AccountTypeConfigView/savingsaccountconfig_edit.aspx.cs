﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.AccountTypeConfigView
{
    public partial class savingsaccountconfig_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["id"] != null)
                {
                    int id = Convert.ToInt32(Session["id"].ToString());
                    SavingsAccountTypeConfigurationLogic satLogic = new SavingsAccountTypeConfigurationLogic();
                    SavingsAccountTypeConfiguration satConfig = satLogic.GetByID(id);

                    txtCIR.Text = satConfig.CreditInterestRate.ToString();
                    txtMinimumBalance.Text = satConfig.MinimumBalance.ToString();

                    GLAccountLogic glaLogic = new GLAccountLogic();
                    List<GLAccount> glAccountList = glaLogic.GetAll();

                    ddlInterestExpenseGL.DataSource = glAccountList;
                    ddlInterestExpenseGL.DataTextField = "Name";
                    ddlInterestExpenseGL.DataValueField = "Id";
                    ddlInterestExpenseGL.DataBind();
                    ddlInterestExpenseGL.Items.FindByValue(satConfig.InterestExpenseGLAccount.Id.ToString()).Selected = true;

                    
                }
                else
                {
                    Response.Redirect("../Home/index.aspx");
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Session["id"].ToString());
            SavingsAccountTypeConfigurationLogic satLogic = new SavingsAccountTypeConfigurationLogic();
            SavingsAccountTypeConfiguration satConfig = satLogic.GetByID(id);

            GLAccountLogic glaLogic = new GLAccountLogic();

            satConfig.CreditInterestRate = Convert.ToDecimal(txtCIR.Text);
            satConfig.MinimumBalance = Convert.ToDecimal(txtMinimumBalance.Text);
            satConfig.InterestExpenseGLAccount = glaLogic.GetByID(Convert.ToInt32(ddlInterestExpenseGL.Value));

            

            satLogic.Update(satConfig);
            CommitTransactions.CommitAll();

            Session["msg"] = "Savings Account Configuration Updated";

            Response.Redirect("savingsaccountconfig.aspx",true);
           
        }
    }
}