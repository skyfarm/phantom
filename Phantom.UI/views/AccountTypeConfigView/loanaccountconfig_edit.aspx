﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Phantom.Master" AutoEventWireup="true" CodeBehind="loanaccountconfig_edit.aspx.cs" Inherits="Phantom.UI.views.AccountTypeConfigView.loanaccountconfig_edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHeader" runat="server">
    <h1>Account Type Configuration Management</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="row">
           <div class="col-md-6 col-md-offset-3">
               <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Loan Account Configuration</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                   
                                        
                                        <div class="form-group">
                                            <label>Debit Interest Rate</label> 
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"  ControlToValidate="txtDIR" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="RegularExpressionValidator" ControlToValidate="txtDIR" ForeColor="Red" Text="Please Enter a Number between 0.0001 and 100" ValidationExpression="^(?!0\d)\d{1,2}(\.\d{1,5})?$" ></asp:RegularExpressionValidator>
                                            <asp:TextBox CssClass="form-control" ID="txtDIR" runat="server" />
                                        </div>
                                         

                                       <div class="form-group">
                                            <label>Interest Income GL Account </label>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator8"  ControlToValidate="ddlInterestIncomeGL" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <select class="form-control" id="ddlInterestIncomeGL" name="ddlInterestIncomeGL" runat="server">
                                                
                                            </select>
                                        </div>

                                       

                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    <asp:Button type="submit" class="btn btn-primary" id ="btnUpdate" name="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click"/>
                                    <%--<button type="submit" class="btn btn-primary" id ="btnUpdate" name="btnUpdate" runat="server">Submit</button>--%>
                                </div>
                            </div>       
              
            </div>                    
    </div>
</asp:Content>
