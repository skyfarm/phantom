﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Phantom.Master" AutoEventWireup="true" CodeBehind="savingsaccountconfig_edit.aspx.cs" Inherits="Phantom.UI.views.AccountTypeConfigView.savingsaccountconfig_edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHeader" runat="server">
    <h1>Account Type Configuration Management</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="row">
           <div class="col-md-6 col-md-offset-3">
               <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Savings Account Configuration</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                   
                                        
                                        <div class="form-group">
                                            <label>Credit Interest Rate</label> 
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"  ControlToValidate="txtCIR" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="RegularExpressionValidator" ControlToValidate="txtCIR" ForeColor="Red" Text="Please Enter a Number between 0.0001 and 100" ValidationExpression="^(?!0\d)\d{1,2}(\.\d{1,5})?$" ></asp:RegularExpressionValidator>
                                            <asp:TextBox CssClass="form-control" ID="txtCIR" runat="server" />
                                        </div>
                                         <div class="form-group">
                                            <label>Minimum Balance</label>
                                             
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator2"  ControlToValidate="txtMinimumBalance" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                             <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="RegularExpressionValidator" ControlToValidate="txtMinimumBalance" ForeColor="Red" Text="Please Enter a Positive Number" ValidationExpression="^[+]?\d+([.]\d+)?$" ></asp:RegularExpressionValidator>
                                             <asp:TextBox CssClass="form-control" ID="txtMinimumBalance" runat="server" />
                                        </div>

                                       <div class="form-group">
                                            <label>Interest Expense GL Account </label>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator8"  ControlToValidate="ddlInterestExpenseGL" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <select class="form-control" id="ddlInterestExpenseGL" name="ddlInterestExpenseGL" runat="server">
                                                
                                            </select>
                                        </div>

                                       

                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    <asp:Button type="submit" class="btn btn-primary" id ="btnUpdate" name="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click"/>
                                    <%--<button type="submit" class="btn btn-primary" id ="btnUpdate" name="btnUpdate" runat="server">Submit</button>--%>
                                </div>
                            </div>       
              
            </div>                    
    </div>
</asp:Content>
