﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.AccountTypeConfigView
{
    public partial class currentaccountconfig_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["id"] != null)
                {
                    int id = Convert.ToInt32(Session["id"].ToString());
                    CurrentAccountTypeConfigurationLogic catLogic = new CurrentAccountTypeConfigurationLogic();
                    CurrentAccountTypeConfiguration catConfig = catLogic.GetByID(id);

                    txtCIR.Text = catConfig.CreditInterestRate.ToString();
                    txtMinimumBalance.Text = catConfig.MinimumBalance.ToString();
                    txtCOT.Text = catConfig.COT.ToString();

                    GLAccountLogic glaLogic = new GLAccountLogic();
                    List<GLAccount> glAccountList = glaLogic.GetAll();

                    ddlInterestExpenseGL.DataSource = glAccountList;
                    ddlInterestExpenseGL.DataTextField = "Name";
                    ddlInterestExpenseGL.DataValueField = "Id";
                    ddlInterestExpenseGL.DataBind();
                    ddlInterestExpenseGL.Items.FindByValue(catConfig.InterestExpenseGLAccount.Id.ToString()).Selected = true;

                    ddlCOTIncomeGL.DataSource = glAccountList;
                    ddlCOTIncomeGL.DataTextField = "Name";
                    ddlCOTIncomeGL.DataValueField = "Id";
                    ddlCOTIncomeGL.DataBind();
                    ddlCOTIncomeGL.Items.FindByValue(catConfig.COTIncomeGLAccount.Id.ToString()).Selected = true;
                }
                else
                {
                    Response.Redirect("../Home/index.aspx");
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Session["id"].ToString());
            CurrentAccountTypeConfigurationLogic catLogic = new CurrentAccountTypeConfigurationLogic();
            CurrentAccountTypeConfiguration catConfig = catLogic.GetByID(id);

            GLAccountLogic glaLogic = new GLAccountLogic();

            catConfig.CreditInterestRate = Convert.ToDecimal(txtCIR.Text);
            catConfig.MinimumBalance = Convert.ToDecimal(txtMinimumBalance.Text);
            catConfig.COT = Convert.ToDecimal(txtCOT.Text);
            catConfig.InterestExpenseGLAccount = glaLogic.GetByID(Convert.ToInt32(ddlInterestExpenseGL.Value));
            catConfig.COTIncomeGLAccount = glaLogic.GetByID(Convert.ToInt32(ddlCOTIncomeGL.Value));
            

            catLogic.Update(catConfig);
            CommitTransactions.CommitAll();

            Session["msg"] = "Current Account Configuration Updated";

            Response.Redirect("currentaccountconfig.aspx",true);
           
        }
    }
}