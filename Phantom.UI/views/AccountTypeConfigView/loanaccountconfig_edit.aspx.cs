﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.AccountTypeConfigView
{
    public partial class loanaccountconfig_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["id"] != null)
                {
                    int id = Convert.ToInt32(Session["id"].ToString());
                    LoanAccountTypeConfigurationLogic latLogic = new LoanAccountTypeConfigurationLogic();
                    LoanAccountTypeConfiguration latConfig = latLogic.GetByID(id);

                    txtDIR.Text = latConfig.DebitInterestRate.ToString();

                    GLAccountLogic glaLogic = new GLAccountLogic();
                    List<GLAccount> glAccountList = glaLogic.GetAll();

                    ddlInterestIncomeGL.DataSource = glAccountList;
                    ddlInterestIncomeGL.DataTextField = "Name";
                    ddlInterestIncomeGL.DataValueField = "Id";
                    ddlInterestIncomeGL.DataBind();
                    ddlInterestIncomeGL.Items.FindByValue(latConfig.InterestIncomeGLAccount.Id.ToString()).Selected = true;

                    
                }
                else
                {
                    Response.Redirect("../Home/index.aspx");
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Session["id"].ToString());
            LoanAccountTypeConfigurationLogic latLogic = new LoanAccountTypeConfigurationLogic();
            LoanAccountTypeConfiguration latConfig = latLogic.GetByID(id);

            GLAccountLogic glaLogic = new GLAccountLogic();

            latConfig.DebitInterestRate = Convert.ToDecimal(txtDIR.Text);
            
            latConfig.InterestIncomeGLAccount = glaLogic.GetByID(Convert.ToInt32(ddlInterestIncomeGL.Value));

            

            latLogic.Update(latConfig);
            CommitTransactions.CommitAll();

            Session["msg"] = "Loan Account Configuration Updated";

            Response.Redirect("loanaccountconfig.aspx",true);
           
        }
    }
}