﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Phantom.Master" AutoEventWireup="true" CodeBehind="generate_card.aspx.cs" Inherits="Phantom.UI.views.CardView.generate_card" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHeader" runat="server">
    <h1>Card Management</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="row">
           <div class="col-md-6 col-md-offset-3">
               <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Generate Card</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                   
                                        <!-- text input -->
                                      

                                    <div class="form-group">
                                            <label>Pick A Customer Account</label>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator7"  ControlToValidate="ddlCustomerAccount" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <select class="form-control" id="ddlCustomerAccount" name="ddlCustomerAccount" runat="server">
                                                
                                            </select>
                                        </div>

                            
                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    <asp:Button type="submit" class="btn btn-primary" id ="btnGenerate" name="btnGenerate" runat="server" Text="Generate Card" OnClick="btnGenerate_Click"/>
                                    <%--<button type="submit" class="btn btn-primary" id ="btnUpdate" name="btnUpdate" runat="server">Submit</button>--%>
                                </div>
                            </div>       
              
            </div>                    
    </div>
</asp:Content>
