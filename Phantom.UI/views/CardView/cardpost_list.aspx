﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Phantom.Master" AutoEventWireup="true" CodeBehind="cardpost_list.aspx.cs" Inherits="Phantom.UI.views.CardView.cardpost_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHeader" runat="server">
     <h1>Card Management</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="row">
                        <div class="col-xs-12">
                            
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">View Card Posts</h3>                                    
                                </div><!-- /.box-header -->
                               
                                
                                <div class="box-body">
                                     <div class="alert alert-success alert-dismissable" id="alertBox" runat="server" visible="false">
                                        <i class="fa fa-check"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <b>Alert!</b><p id="message" runat="server"> Success alert preview. This alert is dismissable.</p>
                                    </div>
                                <asp:GridView CssClass="table table-bordered table-striped" runat="server" ID="dataTable" AutoGenerateColumns="false">
                                   <Columns>
                                       <asp:BoundField DataField="CustomerAccount.AccountName" HeaderText="Account Name" />
                                       <asp:BoundField DataField="CardTransactionType" HeaderText="Transaction Type" />
                                       <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                       <asp:BoundField DataField="Fee" HeaderText="Fee" />
                                       <asp:BoundField DataField="Channel" HeaderText="Channel" />
                                       <asp:BoundField DataField="ToAccount.AccountName" HeaderText="To Account" />
                                       <asp:BoundField DataField="DateCreated" HeaderText="Created" />
                                       <asp:BoundField DataField="DateModified" HeaderText="Modified" />
                                      
                                   </Columns>
                                        
                                </asp:GridView>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                          </div>
                    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
</asp:Content>
