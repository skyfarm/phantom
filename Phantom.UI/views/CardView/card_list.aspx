﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Phantom.Master" AutoEventWireup="true" CodeBehind="card_list.aspx.cs" Inherits="Phantom.UI.views.CardView.card_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHeader" runat="server">
     <h1>Card Management</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="row">
                        <div class="col-xs-12">
                            
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Cards</h3>                                    
                                </div><!-- /.box-header -->
                               
                                
                                <div class="box-body">
                                     <div class="alert alert-success alert-dismissable" id="alertBox" runat="server" visible="false">
                                        <i class="fa fa-check"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <b>Alert!</b><p id="message" runat="server"> Success alert preview. This alert is dismissable.</p>
                                    </div>
                                <asp:GridView CssClass="table table-bordered table-striped" runat="server" ID="dataTable" AutoGenerateColumns="false">
                                   <Columns>
                                       <asp:BoundField DataField="LinkedAccount.AccountName" HeaderText="Account Name" />
                                       <asp:BoundField DataField="LinkedAccount.AccountNumber" HeaderText="Account Number" />
                                       <asp:BoundField DataField="CardPan" HeaderText="Card PAN" />
                                       <asp:BoundField DataField="CVV" HeaderText="CVV" />
                                       <asp:BoundField DataField="ExpiryDate" HeaderText="Expiry Date" />
                                       <asp:BoundField DataField="DateCreated" HeaderText="Created" />
                                       <asp:BoundField DataField="DateModified" HeaderText="Modified" />
                                      
                                   </Columns>
                                        
                                </asp:GridView>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                          </div>
                    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
</asp:Content>
