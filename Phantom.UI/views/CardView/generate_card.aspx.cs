﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.CardView
{
    public partial class generate_card : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if (Session["id"] != null)
                //{
                 
                    List<CustomerAccount> customerAccountsWithoutCard = new CustomerAccountLogic().GetCustomerAccountsWithoutCards();

                    ddlCustomerAccount.DataSource = customerAccountsWithoutCard;
                    ddlCustomerAccount.DataTextField = "AccountName";
                    ddlCustomerAccount.DataValueField = "Id";
                    ddlCustomerAccount.DataBind();
                    ddlCustomerAccount.Items.Insert(0, new ListItem("Select a Customer Account", "", true)); 

                   
                    
                //}
                //else
                //{
                //    Response.Redirect("../Home/index.aspx");
                //}
            }
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
          
            CustomerAccount customerAccount = new CustomerAccountLogic().GetByID(Convert.ToInt32(ddlCustomerAccount.Value));
            Card card = new Card() 
            {
                LinkedAccount = customerAccount
            };
           

            CardLogic cLogic = new CardLogic();
            cLogic.GenerateCardDetails(card);
            cLogic.Insert(card);

            customerAccount.CardIssued = true;
            new CustomerAccountLogic().Update(customerAccount);

            CommitTransactions.CommitAll();

            Session["msg"] = "Card Generated Successfully!";

            Response.Redirect("card_list.aspx",true);
           
        }
    }
}