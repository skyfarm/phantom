﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Phantom.Master" AutoEventWireup="true" CodeBehind="assign_till.aspx.cs" Inherits="Phantom.UI.views.TellerView.assign_till" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHeader" runat="server">
    <h1>Teller Management</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="row">
           <div class="col-md-6 col-md-offset-3">
               <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Assign Till Account to <span id="nameOfUser" runat="server"></span></h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                   
                                        <!-- text input -->
                                      

                                    <div class="form-group">
                                            <label>Pick A Till Account</label>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator7"  ControlToValidate="ddlTill" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <select class="form-control" id="ddlTill" name="ddlTill" runat="server">
                                                
                                            </select>
                                        </div>

                            
                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    <asp:Button type="submit" class="btn btn-primary" id ="btnUpdate" name="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click"/>
                                    <%--<button type="submit" class="btn btn-primary" id ="btnUpdate" name="btnUpdate" runat="server">Submit</button>--%>
                                </div>
                            </div>       
              
            </div>                    
    </div>
</asp:Content>
