﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.TellerView
{
    public partial class assign_till : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["id"] != null)
                {
                    int id = Convert.ToInt32(Session["id"].ToString());
                    UserLogic uLogic = new UserLogic();
                    User user = uLogic.GetByID(id);

                    StringBuilder sb = new StringBuilder();
                    sb.Append(user.Surname.ToString());
                    sb.Append(' ');
                    sb.Append(user.FirstName.ToString());
                    string fullname = sb.ToString().ToUpper();

                    
                   
                    nameOfUser.InnerHtml = fullname;

                    

                    GLAccountLogic glaLogic = new GLAccountLogic();
                    List<GLAccount> glAccountList = glaLogic.GetAllUnAssignedGLAccounts(user.Branch);

                    
                    ddlTill.DataSource = glAccountList;
                    ddlTill.DataTextField = "Name";
                    ddlTill.DataValueField = "Id";
                    ddlTill.DataBind();
                    ddlTill.Items.Insert(0, new ListItem("Select a Till Account", "", true)); 

                   
                    
                }
                else
                {
                    Response.Redirect("../Home/index.aspx");
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Session["id"].ToString());
            UserLogic uLogic = new UserLogic();
            User user = uLogic.GetByID(id);

            GLAccountLogic glaLogic = new GLAccountLogic();

            Teller teller = new Teller() 
            {
                User = user,
                TillAccount = glaLogic.GetByID(Convert.ToInt32(ddlTill.Value))
            };

            TellerLogic tLogic = new TellerLogic();
            tLogic.RegisterTeller(teller);

            //glAccount.Name = txtName.Text;
            //glAccount.Category = glcLogic.GetByID(Convert.ToInt32(ddlCategory.Value));
            //glAccount.Branch = bLogic.GetByID(Convert.ToInt32(ddlBranch.Value));


            CommitTransactions.CommitAll();

            Session["msg"] = "Teller Successfully created!";

            Response.Redirect("user_list.aspx",true);
           
        }
    }
}