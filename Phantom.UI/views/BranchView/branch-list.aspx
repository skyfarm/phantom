﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Phantom.Master" AutoEventWireup="true" CodeBehind="branch-list.aspx.cs" Inherits="Phantom.UI.views.BranchView.branch_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHeader" runat="server">
    <h1>Branch<small>Branch Management</small></h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="row">
                        <div class="col-xs-12">
                            
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Branches</h3>                                    
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <div class="alert alert-success alert-dismissable" id="alertBox" runat="server" visible="false">
                                        <i class="fa fa-check"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <b>Alert!</b><p id="message" runat="server"> Success alert preview. This alert is dismissable.</p>
                                    </div>
                                <asp:GridView CssClass="table table-bordered table-striped" runat="server" ID="dataTable" AutoGenerateColumns="false" DataKeyNames="Id" OnRowCommand="dataTable_RowCommand">
                                   <Columns>
                                       <asp:BoundField DataField="Code" HeaderText="Code" />
                                       <asp:BoundField DataField="Name" HeaderText="Name" />
                                       <asp:BoundField DataField="Status" HeaderText="Status" />
                                       <asp:TemplateField>
                                           <ItemTemplate>
                                               <asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("Id") %>' CommandName="EditRow" >
                                                   <CssClass="btn btn-info"><i class ="ion ion-edit"></i></CssClass></asp:LinkButton>
                                           </ItemTemplate>
                                       </asp:TemplateField>
                                   </Columns>
                                   
                                        
                                </asp:GridView>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                          
                    </div>
    </div>
</asp:Content>
