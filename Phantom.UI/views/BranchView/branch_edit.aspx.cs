﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.BranchView
{
    public partial class branch_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["id"] != null)
                {
                    int id = Convert.ToInt32(Session["id"].ToString());
                    BranchLogic bLogic = new BranchLogic();
                    Branch branch = bLogic.GetByID(id);

                    txtName.Text = branch.Name.ToString();

                    ddlStatus.DataSource = Enum.GetValues(typeof(Status));
                    ddlStatus.DataBind();
                    ddlStatus.Items.FindByValue(branch.Status.ToString()).Selected = true;

                }
                else
                {
                    Response.Redirect("../Home/index.aspx");
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Session["id"].ToString());
            BranchLogic bLogic = new BranchLogic();
            Branch branch = bLogic.GetByID(id);

            branch.Name = txtName.Text;
            branch.Status = (Status)Enum.Parse(typeof(Status), ddlStatus.Value);
          

            

            bLogic.Update(branch);
            CommitTransactions.CommitAll();

            Session["msg"] = "Branch Information Updated";

            Response.Redirect("branch-list.aspx",true);
           
        }
    }
}