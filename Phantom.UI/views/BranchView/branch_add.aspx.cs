﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.BranchView
{
    public partial class branch_add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlStatus.DataSource = Enum.GetValues(typeof(Status));
                ddlStatus.DataBind();
                ddlStatus.Items.Insert(0, new ListItem("Select", "", true));
            }

        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            
            Branch branch = new Branch()
            {
                Name = txtName.Text,
                Status = (Status)Enum.Parse(typeof(Status), ddlStatus.Value)
            };

            BranchLogic bLogic = new BranchLogic();
            bLogic.Insert(branch);
            CommitTransactions.CommitAll();

            Session["msg"] = "Branch Sucessfully  Added";

            Response.Redirect("branch_list.aspx", true);
        }
    }
}