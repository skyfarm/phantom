﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;



namespace Phantom.UI.views.BranchView
{
    public partial class branch_list : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["msg"] != null)
                {
                    alertBox.Visible = true;
                    //alertBox.InnerHtml = Session["msg"].ToString();
                    message.InnerHtml = Session["msg"].ToString();
                }

                BranchLogic bLogic = new BranchLogic();
                List<Branch> list = bLogic.GetAll();

                dataTable.DataSource = list;
                dataTable.DataBind();
            }
           
        }

        protected void dataTable_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditRow")
            {
                Session["Id"] = Convert.ToInt32(e.CommandArgument.ToString());
                //int id = Convert.ToInt32(e.CommandArgument.ToString());
                Response.Redirect("branch_edit.aspx");
            }
        }
    }
}