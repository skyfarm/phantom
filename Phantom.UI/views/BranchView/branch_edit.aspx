﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Phantom.Master" AutoEventWireup="true" CodeBehind="branch_edit.aspx.cs" Inherits="Phantom.UI.views.BranchView.branch_edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHeader" runat="server">
    <h1>General Ledger Category Management</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="row">
           <div class="col-md-6 col-md-offset-3">
               <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Update General Ledger Category Information</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                   
                                        
                                       <div class="form-group">
                                            <label>Name</label> 
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"  ControlToValidate="txtName" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <asp:TextBox CssClass="form-control" ID="txtName" runat="server" />
                                        </div>
                                         <div class="form-group">
                                            <label>Status</label>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator2"  ControlToValidate="ddlStatus" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <select class="form-control" id="ddlStatus" name="ddlCategory" runat="server">
                                                
                                            </select>
                                        </div>

                                       

                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    <asp:Button type="submit" class="btn btn-primary" id ="btnUpdate" name="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click"/>
                                    <%--<button type="submit" class="btn btn-primary" id ="btnUpdate" name="btnUpdate" runat="server">Submit</button>--%>
                                </div>
                            </div>       
              
            </div>                    
    </div>
</asp:Content>
