﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Phantom.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Phantom.UI.views.Home.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHeader" runat="server">
    <h1>Dashboard<small>Control panel</small></h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
     <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3><asp:Label ID ="branchCount" runat="server" /></h3>
                                    <p>Branches</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-home"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    <i class="fa fa-arrow-circle-right" style="visibility:hidden"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3><asp:Label ID ="customerCount" runat="server" /></h3>
                                    <p>Customers</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-stalker"></i>
                                </div>
                               <a href="#" class="small-box-footer">
                                    <i class="fa fa-arrow-circle-right" style="visibility:hidden"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3> <asp:Label ID ="userCount" runat="server" /></h3>
                                    <p>Users </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person"></i>
                                </div>
                               <a href="#" class="small-box-footer">
                                    <i class="fa fa-arrow-circle-right" style="visibility:hidden"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3> <asp:Label ID ="glCategoryCount" runat="server" /></h3>
                                    <p>GL Categories</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-android-folder"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    <i class="fa fa-arrow-circle-right" style="visibility:hidden"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                         <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-orange">
                                <div class="inner">
                                    <h3> <asp:Label ID ="glAccountCount" runat="server" /></h3>
                                    <p>GL Accounts</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-clipboard"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    <i class="fa fa-arrow-circle-right" style="visibility:hidden"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-blue">
                                <div class="inner">
                                    <h3> <asp:Label ID ="tellersCount" runat="server" /></h3>
                                    <p>Tellers</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-android-contact"></i>
                                </div>
                               <a href="#" class="small-box-footer">
                                    <i class="fa fa-arrow-circle-right" style="visibility:hidden"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-purple">
                                <div class="inner">
                                    <h3> <asp:Label ID ="customerAccountCount" runat="server" /></h3>
                                    <p>Customer Accounts</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-briefcase"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    <i class="fa fa-arrow-circle-right" style="visibility:hidden"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                    </div><!-- /.row -->

                   
                 
</asp:Content>
