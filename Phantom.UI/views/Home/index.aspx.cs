﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.Home
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BranchLogic bLogic = new BranchLogic();
            branchCount.Text = bLogic.Count().ToString();

            CustomerLogic cLogic = new CustomerLogic();
            customerCount.Text = cLogic.Count().ToString();

            UserLogic uLogic = new UserLogic();
            userCount.Text = uLogic.Count().ToString();

            GLCategoryLogic glcLogic = new GLCategoryLogic();
            glCategoryCount.Text = glcLogic.Count().ToString();

            GLAccountLogic glaLogic = new GLAccountLogic();
            glAccountCount.Text = glaLogic.Count().ToString();

            TellerLogic tLogic = new TellerLogic();
            tellersCount.Text = tLogic.Count().ToString();

            CustomerAccountLogic caLogic = new CustomerAccountLogic();
            customerAccountCount.Text = caLogic.Count().ToString();
        }
    }
}