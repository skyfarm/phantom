﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.GLAccountView
{
    public partial class glaccount_add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                BranchLogic bLogic = new BranchLogic();
                List<Branch> branchList = bLogic.GetAll();
                ddlBranch.DataSource = branchList;
                ddlBranch.DataTextField = "Name";
                ddlBranch.DataValueField = "Id";
                ddlBranch.DataBind();
                ddlBranch.Items.Insert(0, new ListItem("Select a Branch", "", true));

                GLCategoryLogic glcLogic = new GLCategoryLogic();
                List<GLCategory> glCategoryList = glcLogic.GetAll();
                ddlCategory.DataSource = glCategoryList;
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "Id";
                ddlCategory.DataBind();
                ddlCategory.Items.Insert(0, new ListItem("Select a Category", "", true));
            }

        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            GLCategoryLogic glcLogic = new GLCategoryLogic();
            BranchLogic bLogic = new BranchLogic();
            GLAccount glAccount = new GLAccount()
            {
               Name = txtName.Text,
               Branch = bLogic.GetByID(Convert.ToInt32(ddlBranch.Value)),
               Category = glcLogic.GetByID(Convert.ToInt32(ddlCategory.Value))
            };

            GLAccountLogic glaLogic = new GLAccountLogic();
            glaLogic.GenerateGLACodeAndInsert(glAccount);
            CommitTransactions.CommitAll();

            Session["msg"] = "General Ledger Account Successfully  Added";
            Response.Redirect("glaccount_list.aspx", true);
        }
    }
}