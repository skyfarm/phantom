﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Phantom.Master" AutoEventWireup="true" CodeBehind="glaccount_add.aspx.cs" Inherits="Phantom.UI.views.GLAccountView.glaccount_add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHeader" runat="server">
    <h1>General Ledger Account Management</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
     <div class="row">
           <div class="col-md-6 col-md-offset-3">
               <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Add New General Ledger Account</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                   
                                        <!-- text input -->
                                       <div class="form-group">
                                            <label>Name</label> 
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"  ControlToValidate="txtName" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            
                                            <asp:TextBox CssClass="form-control" ID="txtName" runat="server" />
                                        </div>

                                    <div class="form-group">
                                            <label>Category</label>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator6"  ControlToValidate="ddlCategory" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <select class="form-control" id="ddlCategory" name="ddlCategory" runat="server">
                                                
                                            </select>
                                        </div>

                                    <div class="form-group">
                                            <label>Branch</label>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator7"  ControlToValidate="ddlBranch" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <select class="form-control" id="ddlBranch" name="ddlBranch" runat="server">
                                                
                                            </select>
                                        </div>
                                   
                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    <asp:Button type="submit" class="btn btn-primary" id ="btnInsert" name="btnInsert" runat="server" Text="Add" OnClick="btnInsert_Click"/>
                                    <%--<button type="submit" class="btn btn-primary" id ="btnUpdate" name="btnUpdate" runat="server">Submit</button>--%>
                                </div>
                            </div>       
              
            </div>                    
    </div>
</asp:Content>
