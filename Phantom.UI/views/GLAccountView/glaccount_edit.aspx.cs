﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.GLAccountView
{
    public partial class glaccount_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["id"] != null)
                {
                    int id = Convert.ToInt32(Session["id"].ToString());
                    GLAccountLogic glaLogic = new GLAccountLogic();
                    GLAccount glAccount = glaLogic.GetByID(id);

                    txtName.Text = glAccount.Name.ToString();

                    GLCategoryLogic glcLogic = new GLCategoryLogic();
                    List<GLCategory> glCategoryList = glcLogic.GetAll();
                    ddlCategory.DataSource = glCategoryList;
                    ddlCategory.DataTextField = "Name";
                    ddlCategory.DataValueField = "Id";
                    ddlCategory.DataBind();
                    ddlCategory.Items.FindByText(glAccount.Category.Name.ToString()).Selected = true;

                    BranchLogic bLogic = new BranchLogic();
                    List<Branch> branchList = bLogic.GetAll();
                    ddlBranch.DataSource = branchList;
                    ddlBranch.DataTextField = "Name";
                    ddlBranch.DataValueField = "Id";
                    ddlBranch.DataBind();
                    ddlBranch.Items.FindByText(glAccount.Branch.Name.ToString()).Selected = true;
                    
                }
                else
                {
                    Response.Redirect("../Home/index.aspx");
                }
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Session["id"].ToString());
            GLAccountLogic glaLogic = new GLAccountLogic();
            GLAccount glAccount = glaLogic.GetByID(id);

            GLCategoryLogic glcLogic = new GLCategoryLogic();
            BranchLogic bLogic = new BranchLogic();

            glAccount.Name = txtName.Text;
            glAccount.Category = glcLogic.GetByID(Convert.ToInt32(ddlCategory.Value));
            glAccount.Branch = bLogic.GetByID(Convert.ToInt32(ddlBranch.Value));


           

            glaLogic.Update(glAccount);
            CommitTransactions.CommitAll();

            Session["msg"] = "General Ledger Account Information Updated";

            Response.Redirect("glaccount_list.aspx",true);
           
        }
    }
}