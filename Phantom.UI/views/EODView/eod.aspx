﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Phantom.Master" AutoEventWireup="true" CodeBehind="eod.aspx.cs" Inherits="Phantom.UI.views.EODView.eod" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHeader" runat="server">
    <h1>End Of Day Management</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
     <div class="row">
           <div class="col-md-6 col-md-offset-3">
               <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">End Of Day</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                   
                                     <div class="alert alert-success alert-dismissable" id="successBox" runat="server" visible="false">
                                        <i class="fa fa-check"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <b>Alert!</b><p id="successMessage" runat="server"> Success alert preview. This alert is dismissable.</p>
                                    </div>

                                     <div class="alert alert-danger alert-dismissable" id="failBox" runat="server" visible="false">
                                        <i class="fa fa-check"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <b>Alert!</b><p id="errorMessage" runat="server"> Success alert preview. This alert is dismissable.</p>
                                    </div>

                                       <div class="form-group">
                                           <asp:Button type="submit" class="btn btn-primary btn-lg col-md-offset-3" id ="btnOpenBusiness" name="btnOpenBusiness" runat="server" Text="Open Business"  OnClick="btnOpenBusiness_Click"/>
                                           <asp:Button type="submit" class="btn btn-danger btn-lg" id ="btnCloseBusiness" name="btnCloseBusiness" runat="server" Text="Close Business" OnClick="btnCloseBusiness_Click"/>
                                       </div>

                                <div class="box-footer">
                                    <%--<asp:Button type="submit" class="btn btn-primary" id ="btnInsert" name="btnInsert" runat="server" Text="Add" OnClick="btnInsert_Click"/>--%>
                                    <%--<button type="submit" class="btn btn-primary" id ="btnUpdate" name="btnUpdate" runat="server">Submit</button>--%>
                                </div>
                            </div>       
              
            </div>                    
    </div>
</asp:Content>
