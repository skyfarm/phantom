﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.EODView
{
    public partial class eod : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
               
            //}

            BusinessStatusLogic bsLogic = new BusinessStatusLogic();
            BusinessStatus status = bsLogic.GetByID(1);

            if (status.Status == StateOfBusiness.Open)
            {
                btnOpenBusiness.Visible = false;
                btnCloseBusiness.Visible = true;
            }
            else if (status.Status == StateOfBusiness.Closed)
            {
                btnCloseBusiness.Visible = false;
                btnOpenBusiness.Visible = true;
            }

            if (Session["successMsg"] != null)
            {
                successBox.Visible = true;
                successMessage.InnerHtml = Session["successMsg"].ToString();
            }
            else if (Session["failMsg"] != null)
            {
                failBox.Visible = true;
                errorMessage.InnerHtml = Session["failMsg"].ToString();
            }
        }

        protected void btnOpenBusiness_Click(object sender, EventArgs e)
        {
            if (BusinessStatusLogic.OpenBusiness() == false)
            {
                //business already open
                Session["failMsg"] = "Business is already Open!";

                Response.Redirect("eod.aspx");
            }
            else
            {
                //Business is now Open
                BusinessStatusLogic.OpenBusiness();

                Session["successMsg"] = "Business is now Open!";

                Response.Redirect("eod.aspx");
            }
        }

        protected void btnCloseBusiness_Click(object sender, EventArgs e)
        {
            if (BusinessStatusLogic.CloseBusiness() == false)
            {
                //business already closed
                Session["failMsg"] = "Business is already Closed!";

                Response.Redirect("eod.aspx");
            }
            else
            {
                BusinessStatusLogic.CloseBusiness();

                Session["successMsg"] = "Business is now Closed!";

                Response.Redirect("eod.aspx");
                //Business is now Closed
            }

        }

       
    }
}