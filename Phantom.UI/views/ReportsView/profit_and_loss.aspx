﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Phantom.Master" AutoEventWireup="true" CodeBehind="profit_and_loss.aspx.cs" Inherits="Phantom.UI.views.ReportsView.profit_and_loss" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHeader" runat="server">
    <h1>Reports</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="row">
                        <div class="col-xs-12">
                            
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Profit and Loss</h3>                                    
                                </div><!-- /.box-header -->
                               

                                 <div class="input-group input-group-sm col-sm-3">
                                        <input type="text" class="form-control" runat="server" placeholder="Financial Date YYYY-MM-DD" id="txtDate" name="txtDate"/>
                                        <span class="input-group-btn">
                                            <asp:Button CssClass="btn btn-info btn-flat" name="btnDate" ID="btnDate" runat="server" Text="Go!"  OnClick="btnDate_Click"/>
                                            
                                        </span>
                                    
                                 </div>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator3"  ControlToValidate="txtDate" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="RegularExpressionValidator" ControlToValidate="txtDate" ForeColor="Red" Text="Please use this format YYYY-MM-DD" ValidationExpression="^((19|20)\d\d+)-(0[1-9]|1[012]+)-(0[1-9]|[12][0-9]|3[01])$"></asp:RegularExpressionValidator>
                                    <br/>

                                <asp:GridView CssClass="table table-bordered table-striped" runat="server" ID="dataTable1" AutoGenerateColumns="false" DataKeyNames="Id" OnRowCommand="dataTable_RowCommand">
                                   <Columns>
                                       <asp:BoundField DataField="GLAccount.Name" HeaderText="Income GLs" />
                                       <asp:BoundField DataField="AccountBalanceAtEOD" HeaderText="Balance" />
                                      
                                      
                                   </Columns>

                                         
                                </asp:GridView>

                                <asp:GridView CssClass="table table-bordered table-striped" runat="server" ID="dataTable2" AutoGenerateColumns="false" DataKeyNames="Id" OnRowCommand="dataTable_RowCommand">
                                   <Columns>
                                       <asp:BoundField DataField="GLAccount.Name" HeaderText="Expense GLs" />
                                       <asp:BoundField DataField="AccountBalanceAtEOD" HeaderText="Balance" />
                                   </Columns>

                                         
                                </asp:GridView>

                                <table class="table table-bordered table-striped">
                                    <tbody>
                                        <tr>
                                            <td><strong>Net Income</strong></td>
                                            <td id="netIncome" runat="server"><strong></strong></td>
                                        </tr>
                                    </tbody>
                                </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                          
                    </div>
</asp:Content>
