﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;



namespace Phantom.UI.views.ReportsView
{
    public partial class trial_balance : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
           
        }

        protected void dataTable_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditRow")
            {
                Session["Id"] = Convert.ToInt32(e.CommandArgument.ToString());
                //int id = Convert.ToInt32(e.CommandArgument.ToString());
                Response.Redirect("../Home/index.aspx");
            }
        }

       

        protected void btnDate_Click(object sender, EventArgs e)
        {

            DateTime finDate = Convert.ToDateTime(txtDate.Value);
            GLAccountBalancesAtEODLogic glAccBalLogic = new GLAccountBalancesAtEODLogic();
            List<GLAccountBalancesAtEOD> assetGLs = glAccBalLogic.GetAllByCategory(AccountCategory.Asset, finDate);

            decimal sumOfAllAssets = 0;
            foreach (GLAccountBalancesAtEOD glAccount in assetGLs)
            {
                sumOfAllAssets += glAccount.AccountBalanceAtEOD;
                glAccount.AccountBalanceAtEOD = Math.Round(glAccount.AccountBalanceAtEOD, 2);
            }

            dataTable1.DataSource = assetGLs;
            dataTable1.DataBind();


            List<GLAccountBalancesAtEOD> expenseGLs = glAccBalLogic.GetAllByCategory(AccountCategory.Expense, finDate);
            decimal sumOfAllExpenses = 0;
            foreach (GLAccountBalancesAtEOD glAccount in expenseGLs)
            {
                sumOfAllExpenses += glAccount.AccountBalanceAtEOD;
                glAccount.AccountBalanceAtEOD = Math.Round(glAccount.AccountBalanceAtEOD, 2);
            }

            dataTable2.DataSource = expenseGLs;
            dataTable2.DataBind();


            List<GLAccountBalancesAtEOD> incomeGLs = glAccBalLogic.GetAllByCategory(AccountCategory.Income, finDate);
            decimal sumOfAllIncome = 0;
            foreach (GLAccountBalancesAtEOD glAccount in incomeGLs)
            {
                sumOfAllIncome += glAccount.AccountBalanceAtEOD;
                glAccount.AccountBalanceAtEOD = Math.Round(glAccount.AccountBalanceAtEOD, 2);
            }

            dataTable3.DataSource = incomeGLs;
            dataTable3.DataBind();


            List<GLAccountBalancesAtEOD> liabilityGLs = glAccBalLogic.GetAllByCategory(AccountCategory.Liability, finDate);

            decimal sumOfAllLiabilities = 0;
            foreach (GLAccountBalancesAtEOD glAccount in liabilityGLs)
            {
                sumOfAllLiabilities += glAccount.AccountBalanceAtEOD;
                glAccount.AccountBalanceAtEOD = Math.Round(glAccount.AccountBalanceAtEOD, 2);
            }
            dataTable4.DataSource = liabilityGLs;
            dataTable4.DataBind();


            List<GLAccountBalancesAtEOD> capitalGLs = glAccBalLogic.GetAllByCategory(AccountCategory.Capital, finDate);

            decimal sumOfAllCapitals = 0;
            foreach (GLAccountBalancesAtEOD glAccount in capitalGLs)
            {
                sumOfAllCapitals += glAccount.AccountBalanceAtEOD;
                glAccount.AccountBalanceAtEOD = Math.Round(glAccount.AccountBalanceAtEOD, 2);
            }
            dataTable5.DataSource = capitalGLs;
            dataTable5.DataBind();

            //decimal shareholderaEquity = sumOfAllAssets - sumOfAllLiabilities;
            //netIncome.InnerHtml = shareholderaEquity.ToString();
            decimal sumDebitBalance = sumOfAllAssets + sumOfAllExpenses;
            decimal sumCreditBalance = sumOfAllIncome + sumOfAllLiabilities + sumOfAllCapitals;

            debitBalance.InnerHtml = Math.Round(sumDebitBalance, 2).ToString();
            creditBalance.InnerHtml = Math.Round(sumCreditBalance, 2).ToString();
        }
    }
}