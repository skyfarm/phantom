﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;



namespace Phantom.UI.views.ReportsView
{
    public partial class balance_sheet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               

              

            }
           
        }

        protected void dataTable_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditRow")
            {
                Session["Id"] = Convert.ToInt32(e.CommandArgument.ToString());
                //int id = Convert.ToInt32(e.CommandArgument.ToString());
                Response.Redirect("../Home/index.aspx");
            }
        }

        protected void btnDate_Click(object sender, EventArgs e)
        {
            DateTime finDate = Convert.ToDateTime(txtDate.Value);
            GLAccountBalancesAtEODLogic glAccBalLogic = new GLAccountBalancesAtEODLogic();
            List<GLAccountBalancesAtEOD> assetGLs = glAccBalLogic.GetAllByCategory(AccountCategory.Asset,finDate);

            GLAccountBalancesAtEOD retainedIncomeAssetGL = null; //removed to balance the balance sheet

            foreach (GLAccountBalancesAtEOD glAccount in assetGLs)
            {
                if (glAccount.GLAccount.Id == 1023)
                {
                    retainedIncomeAssetGL = glAccount;
                }
            }

            assetGLs.Remove(retainedIncomeAssetGL);


            decimal sumOfAllAssets = 0;
            foreach (GLAccountBalancesAtEOD glAccount in assetGLs)
            {
                sumOfAllAssets += glAccount.AccountBalanceAtEOD;
                glAccount.AccountBalanceAtEOD = Math.Round(glAccount.AccountBalanceAtEOD, 2);
            }

            dataTable1.DataSource = assetGLs;
            dataTable1.DataBind();
            assetsSum.InnerHtml = "Total Assets = " +  Math.Round(sumOfAllAssets, 2).ToString();

            List<GLAccountBalancesAtEOD> liabilityGLs = glAccBalLogic.GetAllByCategory(AccountCategory.Liability,finDate);

            decimal sumOfAllLiabilities = 0;
            foreach (GLAccountBalancesAtEOD glAccount in liabilityGLs)
            {
                sumOfAllLiabilities += glAccount.AccountBalanceAtEOD;
                glAccount.AccountBalanceAtEOD = Math.Round(glAccount.AccountBalanceAtEOD, 2);
            }
            dataTable2.DataSource = liabilityGLs;
            dataTable2.DataBind();

            liabilitiesSum.InnerHtml = "Total Liabilities = " + Math.Round(sumOfAllLiabilities, 2).ToString();

            //decimal shareholderaEquity = sumOfAllAssets - sumOfAllLiabilities;
            //netIncome.InnerHtml = shareholderaEquity.ToString();

            //decimal sumOfLiabilitesAndCapital = 
            List<GLAccountBalancesAtEOD> capitalGLs = glAccBalLogic.GetAllByCategory(AccountCategory.Capital, finDate);
            decimal sumOfAllCapital = 0;
            foreach (GLAccountBalancesAtEOD glAccount in capitalGLs)
            {
                sumOfAllCapital += glAccount.AccountBalanceAtEOD;
                glAccount.AccountBalanceAtEOD = Math.Round(glAccount.AccountBalanceAtEOD, 2);
            }
            dataTable3.DataSource = capitalGLs;
            dataTable3.DataBind();
            capitalsSum.InnerHtml = "Total Capital = " + Math.Round(sumOfAllCapital, 2).ToString();

            decimal sumOfCapitalAndLiabilityAccounts = sumOfAllCapital + sumOfAllLiabilities;

            sumOfLiabilityAndCapital.InnerHtml = "Sum of Capital and Liability = " + Math.Round(sumOfCapitalAndLiabilityAccounts, 2).ToString();
        }
    }
}