﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;



namespace Phantom.UI.views.ReportsView
{
    public partial class profit_and_loss : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               


            }
           
        }

        protected void dataTable_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditRow")
            {
                Session["Id"] = Convert.ToInt32(e.CommandArgument.ToString());
                //int id = Convert.ToInt32(e.CommandArgument.ToString());
                Response.Redirect("../Home/index.aspx");
            }
        }

        protected void btnDate_Click(object sender, EventArgs e)
        {

            DateTime finDate = Convert.ToDateTime(txtDate.Value);
            GLAccountBalancesAtEODLogic glAccBalLogic = new GLAccountBalancesAtEODLogic();
            List<GLAccountBalancesAtEOD> incomeGLs = glAccBalLogic.GetAllByCategory(AccountCategory.Income,finDate);

            decimal sumOfAllIncome = 0;
            foreach (GLAccountBalancesAtEOD glAccount in incomeGLs)
            {
                sumOfAllIncome += glAccount.AccountBalanceAtEOD;
                glAccount.AccountBalanceAtEOD = Math.Round(glAccount.AccountBalanceAtEOD, 2);
            }

            dataTable1.DataSource = incomeGLs;
            dataTable1.DataBind();


            List<GLAccountBalancesAtEOD> expenseGLs = glAccBalLogic.GetAllByCategory(AccountCategory.Expense,finDate);

            decimal sumOfAllExpenses = 0;
            foreach (GLAccountBalancesAtEOD glAccount in expenseGLs)
            {
                sumOfAllExpenses += glAccount.AccountBalanceAtEOD;
                glAccount.AccountBalanceAtEOD = Math.Round(glAccount.AccountBalanceAtEOD, 2);
            }
            dataTable2.DataSource = expenseGLs;
            dataTable2.DataBind();

            decimal net = sumOfAllIncome - sumOfAllExpenses;
            netIncome.InnerHtml = Math.Round(net,2).ToString();
        }
    }
}