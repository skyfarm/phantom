﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.GLPostView
{
    public partial class glpost_add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BusinessStatusLogic bsLogic = new BusinessStatusLogic();
            BusinessStatus status = bsLogic.GetByID(1);

            if (status.Status == StateOfBusiness.Closed)
            {
                //redirect
                Session["err"] = "Posting is restricted when Business is Closed!";
                Response.Redirect("glpost_list.aspx");
            }
            if (!IsPostBack)
            {
                GLAccountLogic glaLogic = new GLAccountLogic();
                List<GLAccount> listOfGLAAccounts = glaLogic.GetAll();
               
                var datasource = from x in listOfGLAAccounts
                select new
                {
                   DisplayField = string.Format("{0} - {1}", x.Code, x.Name),
                   x.Id
                };

                ddlGLADebit.DataSource = datasource;
                ddlGLADebit.DataTextField = "DisplayField";
                ddlGLADebit.DataValueField = "Id";
                ddlGLADebit.DataBind();
                ddlGLADebit.Items.Insert(0, new ListItem("Select GL Account to Debit", "", true));

                ddlGLACredit.DataSource = datasource;
                ddlGLACredit.DataTextField = "DisplayField";
                ddlGLACredit.DataValueField = "Id";
                ddlGLACredit.DataBind();
                ddlGLACredit.Items.Insert(0, new ListItem("Select GL Account to Credit", "", true));
  
            }
            else
            {
                if (Session["err"] != null)
                {
                    alertBox.Visible = false;
                    message.InnerHtml = Session["err"].ToString();
                }
            }

        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            decimal amount = Convert.ToDecimal(txtCreditAmount.Text);
            GLAccountLogic glcLogic = new GLAccountLogic();
            GLAccount debitGL = glcLogic.GetByID(Convert.ToInt32(ddlGLADebit.Value));
            GLAccount creditGL = glcLogic.GetByID(Convert.ToInt32(ddlGLACredit.Value));

            GLPostLogic glpLogic = new GLPostLogic();

            if (glpLogic.IsFundsSufficient(debitGL, amount) == true)
            {
                //debit the Source GL, credit the destination GL
                glpLogic.DebitAccount(debitGL, amount);
                glpLogic.CreditAccount(creditGL, amount);
            }
            else
            {
                //Insufficient Balance
                Session["err"] = "Insufficient Funds in the GL";
                Response.Redirect("glpost_add.aspx");
            }

            GLPost glPost = new GLPost() 
            {
                GLAccountToDebit = debitGL,
                DebitAmount = amount,
                GLAccountToCredit = creditGL,
                CreditAmount = amount,
                Narration = txtNarration.Value
            };

            glpLogic.Insert(glPost);

            CommitTransactions.CommitAll();

            Session["msg"] = "Transaction Successfully Posted";

            Response.Redirect("glpost_list.aspx", true);
        }
    }
}