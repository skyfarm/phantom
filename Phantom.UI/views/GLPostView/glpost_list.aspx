﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Phantom.Master" AutoEventWireup="true" CodeBehind="glpost_list.aspx.cs" Inherits="Phantom.UI.views.GLPostView.glpost_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHeader" runat="server">
     <h1>General Ledger Account Management</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="row">
                        <div class="col-xs-12">
                            
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">General Ledger Accounts</h3>                                    
                                </div><!-- /.box-header -->
                               
                                
                                <div class="box-body">
                                     <div class="alert alert-success alert-dismissable" id="alertBox" runat="server" visible="false">
                                        <i class="fa fa-check"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <b>Alert!</b><p id="message" runat="server"> Success alert preview. This alert is dismissable.</p>
                                    </div>
                                     <div class="alert alert-danger alert-dismissable" id="errAlertBox" runat="server" visible="false">
                                         <i class="fa fa-ban"></i>
                                         <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                         <b>Alert!</b><p id="errMessage" runat="server">Lorem Ipsum</p>
                                    </div>
                                <asp:GridView CssClass="table table-bordered table-striped" runat="server" ID="dataTable" AutoGenerateColumns="false" DataKeyNames="Id" OnRowCommand="dataTable_RowCommand">
                                   <Columns>
                                       <asp:BoundField DataField="GLAccountToDebit.Name" HeaderText="Debit GL" />
                                       <asp:BoundField DataField="GLAccountToDebit.Code" HeaderText="GL Code" />
                                       <asp:BoundField DataField="DebitAmount" HeaderText="Debit Amount" />
                                       <asp:BoundField DataField="GLAccountToCredit.Name" HeaderText="Credit GL" />
                                       <asp:BoundField DataField="GLAccountToCredit.Code" HeaderText="GL Code" />
                                       <asp:BoundField DataField="CreditAmount" HeaderText="Credit Amount" />
                                       <asp:BoundField DataField="Narration" HeaderText="Narration" />
                                       <asp:BoundField DataField="DateCreated" HeaderText="Date" />
                                       
                                   </Columns>
                                        
                                </asp:GridView>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                          </div>
                    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
</asp:Content>
