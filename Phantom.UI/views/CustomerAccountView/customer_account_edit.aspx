﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Phantom.Master" AutoEventWireup="true" CodeBehind="customer_account_edit.aspx.cs" Inherits="Phantom.UI.views.CustomerAccountView.customer_account_edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHeader" runat="server">
    <h1>Customer Account Management</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="row">
           <div class="col-md-6 col-md-offset-3">
               <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Update Customer Account Infomation</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                   
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Account Name</label> 
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"  ControlToValidate="txtAccName" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <%--<input type="text" class="form-control" placeholder="Enter ..." id ="txtFirstname" name="txtFirstname" runat="server" />--%>
                                            <asp:TextBox CssClass="form-control" ID="txtAccName" runat="server" />
                                        </div>
                      

                                     <div class="form-group">
                                            <label>Branch</label>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator8"  ControlToValidate="ddlBranch" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <select class="form-control" id="ddlBranch" name="ddlBranch" runat="server">
                                                
                                            </select>
                                        </div>

                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    <asp:Button type="submit" class="btn btn-primary" id ="btnUpdate" name="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click"/>
                                    <%--<button type="submit" class="btn btn-primary" id ="btnUpdate" name="btnUpdate" runat="server">Submit</button>--%>
                                </div>
                            </div>       
              
            </div>                    
    </div>
</asp:Content>
