﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.CustomerAccountView
{
    public partial class customer_account_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["id"] != null)
                {
                    int id = Convert.ToInt32(Session["id"].ToString());
                    CustomerAccountLogic caLogic = new CustomerAccountLogic();
                    CustomerAccount cAccount = caLogic.GetByID(id);

                    BranchLogic bLogic = new BranchLogic();
                    List<Branch> branchList = bLogic.GetAll();

                    txtAccName.Text = cAccount.AccountName.ToString();

                    

                    ddlBranch.DataSource = branchList;
                    ddlBranch.DataTextField = "Name";
                    ddlBranch.DataValueField = "Id";
                    ddlBranch.DataBind();
                    ddlBranch.Items.FindByText(cAccount.Branch.Name.ToString()).Selected = true;
                    
                    
                   
                }
                else
                {
                    Response.Redirect("../Home/index.aspx");
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Session["id"].ToString());
            CustomerAccountLogic uLogic = new CustomerAccountLogic();
            CustomerAccount cAccount = uLogic.GetByID(id);

            BranchLogic bLogic = new BranchLogic();

            cAccount.AccountName = txtAccName.Text;
            cAccount.Branch = bLogic.GetByID(Convert.ToInt32(ddlBranch.Value));

            uLogic.Update(cAccount);
            CommitTransactions.CommitAll();

            Session["msg"] = "Customer Account Information Updated";

            Response.Redirect("customer_account_list.aspx",true);
           
        }
    }
}