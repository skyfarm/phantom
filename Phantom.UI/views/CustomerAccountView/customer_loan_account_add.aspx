﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Phantom.Master" AutoEventWireup="true" CodeBehind="customer_loan_account_add.aspx.cs" Inherits="Phantom.UI.views.CustomerAccountView.customer_loan_account_add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHeader" runat="server">
    <h1>Customer Account Management</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
     <div class="row">
           <div class="col-md-6 col-md-offset-3">
               <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Create Account For <span id="nameOfCustomer" runat="server"></span></h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                   
                                        <!-- text input -->
                                       <%--<div class="form-group">
                                            <label>Customer</label>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1"  ControlToValidate="ddlCustomer" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <select class="form-control" id="ddlCustomer" name="ddlCustomer" runat="server">
                                                
                                            </select>
                                        </div>--%>
                                    <div class="form-group">
                                            <label>Linked Account</label>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator7"  ControlToValidate="ddlLinkedAccount" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <select class="form-control" id="ddlLinkedAccount" name="ddlLinkedAccount" runat="server">
                                                
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Branch</label>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator8"  ControlToValidate="ddlBranch" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <select class="form-control" id="ddlBranch" name="ddlBranch" runat="server">
                                                
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Account Name</label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2"  ControlToValidate="txtAccName" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                             <asp:TextBox CssClass="form-control" ID="txtAccName" runat="server" />
                                        </div>

                                    <div class="form-group">
                                            <label>Loan Amount</label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"  ControlToValidate="txtLoanAmount" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="RegularExpressionValidator" ControlToValidate="txtLoanAmount" ForeColor="Red" Text="Please Enter a Positive Number" ValidationExpression="^[+]?\d+([.]\d+)?$" ></asp:RegularExpressionValidator>     
                                        <asp:TextBox CssClass="form-control" ID="txtLoanAmount" runat="server" />
                                        </div>

                                       
                                
                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    <asp:Button type="submit" class="btn btn-primary" id ="btnInsert" name="btnInsert" runat="server" Text="Add" OnClick="btnInsert_Click"/>
                                    <%--<button type="submit" class="btn btn-primary" id ="btnUpdate" name="btnUpdate" runat="server">Submit</button>--%>
                                </div>
                            </div>       
              
            </div>                    
    </div>
</asp:Content>
