﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.CustomerAccountView
{
    public partial class customer_account_add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["id"] != null)
                {
                    int id = Convert.ToInt32(Session["id"].ToString());
                    CustomerLogic cLogic = new CustomerLogic();
                    Customer customer = cLogic.GetByID(id);

                    StringBuilder sb = new StringBuilder();
                    sb.Append(customer.Surname.ToString());
                    sb.Append(' ');
                    sb.Append(customer.FirstName.ToString());
                    string fullname = sb.ToString().ToUpper();

                    nameOfCustomer.InnerHtml = fullname;

                   
                    BranchLogic bLogic = new BranchLogic();
                    List<Branch> listOfBranches = bLogic.GetAll();
                    ddlBranch.DataSource = listOfBranches;
                    ddlBranch.DataTextField = "Name";
                    ddlBranch.DataValueField = "Id";
                    ddlBranch.DataBind();
                    ddlBranch.Items.Insert(0, new ListItem("Select a Branch", "", true));

                    ddlAccountType.DataSource = Enum.GetValues(typeof(AccountType));
                    ddlAccountType.DataBind();
                    ddlAccountType.Items.Remove("Loan");
                    ddlAccountType.Items.Insert(0, new ListItem("Select Account Type", "", true));
                }
                else
                {
                    Response.Redirect("../Home/index.aspx");
                }
            }

        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            CustomerLogic cLogic = new CustomerLogic();
            BranchLogic bLogic = new BranchLogic();
           
            CustomerAccount cAccount = new CustomerAccount()
            {
                Customer = cLogic.GetByID(Convert.ToInt32(Session["id"].ToString())),
                Branch = bLogic.GetByID(Convert.ToInt32(ddlBranch.Value)),
                AccountName = txtAccName.Text,
                AccountType = (AccountType)Enum.Parse(typeof(AccountType),ddlAccountType.Value),
               
            };

            CustomerAccountLogic caLogic = new CustomerAccountLogic();
            caLogic.CreateAccount(cAccount);
            CommitTransactions.CommitAll();

            Session["msg"] = "Customer Account Successfully Created";

            Response.Redirect("customer_account_list.aspx", true);
        }
    }
}