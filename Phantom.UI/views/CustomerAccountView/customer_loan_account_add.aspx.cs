﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Phantom.Core;
using Phantom.Logic;
using System.Data;

namespace Phantom.UI.views.CustomerAccountView
{
    public partial class customer_loan_account_add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["id"] != null)
                {
                    
                    int id = Convert.ToInt32(Session["id"].ToString());
                    CustomerLogic cLogic = new CustomerLogic();
                    Customer customer = cLogic.GetByID(id);

                    StringBuilder sb = new StringBuilder();
                    sb.Append(customer.Surname.ToString());
                    sb.Append(' ');
                    sb.Append(customer.FirstName.ToString());
                    string fullname = sb.ToString().ToUpper();

                    nameOfCustomer.InnerHtml = fullname;

                    CustomerAccountLogic caLogic = new CustomerAccountLogic();
                    
                    List<CustomerAccount> list = caLogic.GetACustomersAccounts(customer);
                    var datasource = from x in list
                    select new
                    {
                        DisplayField = string.Format("{0} ({1})", x.AccountName, x.AccountType),
                        x.Id
                    };
                    
                   
                    ddlLinkedAccount.DataSource = datasource;
                    ddlLinkedAccount.DataTextField = "DisplayField";
                    ddlLinkedAccount.DataValueField = "Id";
                    ddlLinkedAccount.DataBind();
                   
                    BranchLogic bLogic = new BranchLogic();
                    List<Branch> listOfBranches = bLogic.GetAll();
                    ddlBranch.DataSource = listOfBranches;
                    ddlBranch.DataTextField = "Name";
                    ddlBranch.DataValueField = "Id";
                    ddlBranch.DataBind();
                    ddlBranch.Items.Insert(0, new ListItem("Select a Branch", "", true));

                }
                else
                {
                    Response.Redirect("../Home/index.aspx");
                }
            }

        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            CustomerLogic cLogic = new CustomerLogic();
            BranchLogic bLogic = new BranchLogic();

            decimal loanAmount = Convert.ToDecimal(txtLoanAmount.Text);
            CustomerAccount cAccount = new CustomerAccount()
            {
                Customer = cLogic.GetByID(Convert.ToInt32(Session["id"].ToString())),
                Branch = bLogic.GetByID(Convert.ToInt32(ddlBranch.Value)),
                AccountName = txtAccName.Text,
                AccountType = AccountType.Loan,
                AccountBalance = loanAmount
               
            };

            

            CustomerAccountLogic caLogic = new CustomerAccountLogic();
            CustomerAccount linkedAccount = caLogic.GetByID(Convert.ToInt32(ddlLinkedAccount.Value));
            Loan loan = new Loan()
            {
                Terms = "Lorem Ipsum Dolor",
                Amount = loanAmount,
                LinkedAccount = linkedAccount
            };
            caLogic.CreateLoan(cAccount,loan);

            GLAccountLogic glaLogic = new GLAccountLogic();
            GLAccount loanGL = glaLogic.GetByID(1019);
            loanGL.AccountBalance += loanAmount;
            glaLogic.Update(loanGL);

            GLAccount accountsPayable = glaLogic.GetByID(1020);
            accountsPayable.AccountBalance += loanAmount;
            glaLogic.Update(accountsPayable);

            //GLPostLogic glpLogic = new GLPostLogic();
            //glpLogic.Insert(new GLPost() 
            //{
            //    GLAccountToDebit = vault,
            //    GLAccountToCredit = loanGL,
            //    CreditAmount = loanAmount,
            //    Narration = "New Loan"
            //});

           
            CommitTransactions.CommitAll();

            Session["msg"] = "Customer Account Successfully Created";

            Response.Redirect("customer_account_list.aspx", true);
        }
    }
}