﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.CustomerView
{
    public partial class customer_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["id"] != null)
                {
                    int id = Convert.ToInt32(Session["id"].ToString());
                    CustomerLogic cLogic = new CustomerLogic();
                    Customer customer = cLogic.GetByID(id);


                    txtFirstname.Text = customer.FirstName.ToString();
                    txtSurname.Text = customer.Surname.ToString();
                    txtEmail.Text = customer.Email.ToString();
                    txtPhone.Text = customer.Phone.ToString();

                    ddlGender.DataSource = Enum.GetValues(typeof(Gender));
                    ddlGender.DataBind();
                    ddlGender.Items.FindByValue(customer.Gender.ToString()).Selected = true;
                }
                else
                {
                    Response.Redirect("../Home/index.aspx");
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Session["id"].ToString());
            CustomerLogic cLogic = new CustomerLogic();
            Customer customer = cLogic.GetByID(id);

            customer.FirstName = txtFirstname.Text;
            customer.Surname = txtSurname.Text;
            customer.Email = txtEmail.Text;
            customer.Phone = txtPhone.Text;
            customer.Gender = (Gender)Enum.Parse(typeof(Gender), ddlGender.Value);

            cLogic.Update(customer);
            CommitTransactions.CommitAll();

            Session["msg"] = "Customer Information Updated";

            Response.Redirect("customer_list.aspx",true);
           
        }
    }
}