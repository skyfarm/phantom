﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.CustomerView
{
    public partial class customer_add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlGender.DataSource = Enum.GetValues(typeof(Gender));
                ddlGender.DataBind();
                ddlGender.Items.Insert(0, new ListItem("Select Gender", "", true));
            }

        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            Customer customer = new Customer()
            {
                FirstName = txtFirstname.Text,
                Surname = txtSurname.Text,
                Email = txtEmail.Text,
                Phone = txtPhone.Text,
                Gender = (Gender)Enum.Parse(typeof(Gender), ddlGender.Value)
            };

            CustomerLogic cLogic = new CustomerLogic();
            if (cLogic.CheckIfCustomerExists(customer.Phone, customer.Email) == true)
            {
                //customer alreeady exists
                Session["err"] = "Customer Already Exists, Try differnt Phone Number and/or  email";

                Response.Redirect("customer_list.aspx", true);
            }
            else
            {
                cLogic.Insert(customer);
                CommitTransactions.CommitAll();

                Session["msg"] = "Customer Successfully  Added";

                Response.Redirect("customer_list.aspx", true);
            }
            
        }
    }
}