﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;
using System.Text;

namespace Phantom.UI.views.TellerPostView
{
    public partial class customer_account_post : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BusinessStatusLogic bsLogic = new BusinessStatusLogic();
            BusinessStatus status = bsLogic.GetByID(1);

            if (status.Status == StateOfBusiness.Closed)
            {
                //redirect
            }
            if (!IsPostBack)
            {
                if (Session["id"] != null)
                {
                    int id = Convert.ToInt32(Session["id"].ToString());
                    CustomerAccountLogic caLogic = new CustomerAccountLogic();
                    CustomerAccount customerAccount = caLogic.GetByID(id);

                    StringBuilder sb = new StringBuilder();
                    sb.Append(customerAccount.AccountNumber.ToString());
                    sb.Append('-');
                    sb.Append(customerAccount.AccountName.ToString());
                    string text = sb.ToString().ToUpper();

                    accountInfo.InnerHtml = text;

                    ddlPostType.DataSource = Enum.GetValues(typeof(PostingType));
                    ddlPostType.DataBind();
                    ddlPostType.Items.Insert(0, new ListItem("Select", "", true));
                }

            }
            else
            {
                if (Session["err"] != null)
                {
                    alertBox.Visible = false;
                    message.InnerHtml = Session["err"].ToString();
                }
                //else
                //{
                //    Response.Redirect("../Home/index.aspx")
                //}
                 
            }

        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
           

            if (Session["id"] != null)
            {
                int id = Convert.ToInt32(Session["id"].ToString());
                CustomerAccountLogic caLogic = new CustomerAccountLogic();
                CustomerAccount customerAccount = caLogic.GetByID(id);

                decimal amount = Convert.ToDecimal(txtAmount.Text);
                User user = (User)Session["user"];

                TellerLogic tLogic = new TellerLogic();
                Teller teller = tLogic.GetTellerInformationByUserID(user);
                GLAccount tillAccount = teller.TillAccount;

                TellerPostLogic tpLogic = new TellerPostLogic();



                if (ddlPostType.Value.Equals(Enum.GetName(typeof(PostingType),PostingType.Deposit)))
                {
                    //deposit, debit the GL credit the Customer account
                    if(tpLogic.IsFundsSufficientInTillAccount(tillAccount, amount))
                    {
                        //deposit
                        tpLogic.DepositPost(tillAccount,customerAccount,amount);
                        //redirect
                        Session["msg"] = "Post Successful";
                    }
                    else
                    {
                        Session["err"] = "Insufficient Funds in Till Account";
                        //redirect
                    }
                    
                }
                else if (ddlPostType.Value.Equals(Enum.GetName(typeof(PostingType), PostingType.Withdraw)))
                {
                    //withdraw, debit the Customer Account, credit the GL
                    //Check for COT
                    if (customerAccount.AccountType == AccountType.Current)
                    {
                        CurrentAccountTypeConfigurationLogic catLogic = new CurrentAccountTypeConfigurationLogic();
                        CurrentAccountTypeConfiguration config = catLogic.GetByID(1);

                        decimal cotValue = (config.COT / 100) * amount;

                        amount += cotValue;

                    }
                    if (tpLogic.IsFundsSufficientInCustomerAccount(customerAccount, amount))
                    {
                        //withdraw
                        tpLogic.WithdrawPost(tillAccount, customerAccount, amount);

                       
                        //redirect
                        Session["msg"] = "Post Successful";
                    }
                    else
                    {   
                        Session["err"] = "Insufficient Funds in Customer Account";
                        //redirect
                    }
                }

                TellerPost tPost = new TellerPost()
                {
                    PostType = (PostingType)Enum.Parse(typeof(PostingType), ddlPostType.Value),
                    Amount = amount,
                    Narration = txtNarration.Value,
                    CustomerAccount = customerAccount,
                    Teller = teller
                };

                tpLogic.Insert(tPost);
               
            }


            

            CommitTransactions.CommitAll();

            Session["msg"] = "Transaction Successfully Posted";

            Response.Redirect("teller_post_list.aspx", true);
        }
    }
}