﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.TellerPostView
{
    public partial class teller_post_list : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                if (Session["msg"] != null)
                {
                    alertBox.Visible = true;
                    //alertBox.InnerHtml = Session["msg"].ToString();
                    message.InnerHtml = Session["msg"].ToString();
                }
                else if (Session["err"] != null)
                {
                    errAlertBox.Visible = true;
                    errMessage.InnerHtml = Session["err"].ToString();
                }
                TellerPostLogic glpLogic =  new TellerPostLogic();
                List<TellerPost> list = glpLogic.GetAll();

                dataTable.DataSource = list;
                dataTable.DataBind();
            }
        }

        
    }
}