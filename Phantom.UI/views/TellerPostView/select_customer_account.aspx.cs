﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Phantom.Core;
using Phantom.Logic;

namespace Phantom.UI.views.TellerPostView
{
    public partial class select_customer_account : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BusinessStatusLogic bsLogic = new BusinessStatusLogic();
            BusinessStatus status = bsLogic.GetByID(1);

            if (status.Status == StateOfBusiness.Closed)
            {
                //redirect
                Session["err"] = "Posting is restricted when Business is Closed!";
                Response.Redirect("teller_post_list.aspx");
            }
            if (!IsPostBack)
            {
                if (Session["msg"] != null)
                {
                    alertBox.Visible = true;
                    //alertBox.InnerHtml = Session["msg"].ToString();
                    message.InnerHtml = Session["msg"].ToString();
                }

                if (Session["user"] != null)
                {
                    User user = (User)Session["user"];
                    CustomerAccountLogic caLogic = new CustomerAccountLogic();
                    List<CustomerAccount> list;

                    if (user.Level == AdminLevel.SuperUser)
                    {
                        list = caLogic.GetAllActiveCustomerAccounts();
                    }
                    else
                    {
                        list = caLogic.GetActiveCustomerAccountsInABranch(user.Branch);
                    }


                    dataTable.DataSource = list;
                    dataTable.DataBind();
                }
              
            }
        }

        protected void dataTable_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditRow")
            {
                Session["Id"] = e.CommandArgument.ToString();
                Response.Redirect("customer_account_post.aspx");
            }
        }
    }
}