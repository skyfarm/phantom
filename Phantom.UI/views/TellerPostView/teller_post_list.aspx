﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Phantom.Master" AutoEventWireup="true" CodeBehind="teller_post_list.aspx.cs" Inherits="Phantom.UI.views.TellerPostView.teller_post_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHeader" runat="server">
     <h1>Teller Posting </h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="row">
                        <div class="col-xs-12">
                            
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Postings</h3>                                    
                                </div><!-- /.box-header -->
                               
                                
                                <div class="box-body">
                                     <div class="alert alert-success alert-dismissable" id="alertBox" runat="server" visible="false">
                                        <i class="fa fa-check"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <b>Alert!</b><p id="message" runat="server"> Success alert preview. This alert is dismissable.</p>
                                    </div>
                                    <div class="alert alert-danger alert-dismissable" id="errAlertBox" runat="server" visible="false">
                                         <i class="fa fa-ban"></i>
                                         <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                         <b>Alert!</b><p id="errMessage" runat="server">Lorem Ipsum</p>
                                    </div>
                                <asp:GridView CssClass="table table-bordered table-striped" runat="server" ID="dataTable" AutoGenerateColumns="false" DataKeyNames="Id" >
                                   <Columns>
                                       <asp:BoundField DataField="CustomerAccount.AccountName" HeaderText="Customer Account" />
                                       <asp:BoundField DataField="PostType" HeaderText="Type" />
                                       <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                       <asp:BoundField DataField="Narration" HeaderText="Narration" />
                                       <asp:BoundField DataField="Teller.TillAccount.Name" HeaderText="Till Account" /> 
                                       <asp:BoundField DataField="DateCreated" HeaderText="Date" />
                                       
                                   </Columns>
                                        
                                </asp:GridView>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                          </div>
                    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
</asp:Content>
