﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Phantom.Master" AutoEventWireup="true" CodeBehind="customer_account_post.aspx.cs" Inherits="Phantom.UI.views.TellerPostView.customer_account_post" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHeader" runat="server">
    <h1>General Ledger Account Posting </h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
     <div class="row">
           <div class="col-md-6 col-md-offset-3">
               <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">New Post Transaction on <span id="accountInfo" runat="server"></span></h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">

                                    <div class="alert alert-danger alert-dismissable" id="alertBox" runat="server" visible="false">
                                         <i class="fa fa-ban"></i>
                                         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                         <b>Alert!</b><p id="message" runat="server">Lorem Ipsum</p>
                                    </div>
                                   
                                         <div class="form-group">
                                            <label>Post Type</label>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator9"  ControlToValidate="ddlPostType" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <select class="form-control" id="ddlPostType" name="ddlPostType" runat="server">
                                                
                                            </select>
                                        </div>

                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Amount</label> 
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"  ControlToValidate="txtAmount" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="RegularExpressionValidator" ControlToValidate="txtAmount" ForeColor="Red" Text="Please Enter a Positive Number" ValidationExpression="^[+]?\d+([.]\d+)?$" ></asp:RegularExpressionValidator>
                                            <asp:TextBox CssClass="form-control" ID="txtAmount" runat="server" />
                                        </div>
                                        

                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Confirm Amount</label> 
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3"  ControlToValidate="txtConfirmAmount" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                             <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="RegularExpressionValidator" ControlToValidate="txtAmount" ForeColor="Red" Text="Please Enter a Positive Number" ValidationExpression="^[+]?\d+([.]\d+)?$" ></asp:RegularExpressionValidator>
                                            <asp:CompareValidator runat="server" id="cmpNumbers" controltovalidate="txtConfirmAmount" controltocompare="txtAmount" operator="Equal" type="Integer" errormessage="Debit amount must equal Credit amount"  ForeColor="Red"/>
                                            <asp:TextBox CssClass="form-control" ID="txtConfirmAmount" runat="server" />
                                        </div>

                                        <div class="form-group">
                                            <label>Narration</label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4"  ControlToValidate="txtNarration" runat="server" ErrorMessage=" Required!" ForeColor="red"></asp:RequiredFieldValidator>
                                            <textarea class="form-control" rows="3" placeholder="Enter ..." id="txtNarration" runat="server"></textarea>
                                        </div>
                                   
                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    <asp:Button type="submit" class="btn btn-primary" id ="btnInsert" name="btnInsert" runat="server" Text="Add" OnClick="btnInsert_Click"/>
                                    <%--<button type="submit" class="btn btn-primary" id ="btnUpdate" name="btnUpdate" runat="server">Submit</button>--%>
                                </div>
                            </div>       
              
            </div>                    
    </div>
</asp:Content>
