﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Phantom.Core;
using Phantom.Logic;
namespace Phantom.UI
{
    public partial class Phantom : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                User user = (User)Session["user"];
                
                
                StringBuilder sb = new StringBuilder();
                sb.Append(user.FirstName.ToString());
                sb.Append(' ');
                sb.Append(user.Surname.ToString());

                usernames.InnerHtml = sb.ToString();

                sb.Append(" - ");
                sb.Append(user.Level.ToString());

                nameAndRole.InnerHtml = sb.ToString();
                
                
                if (user.Level == AdminLevel.SuperUser){ branchNav.Visible = true; }
                if (user.Level == AdminLevel.SuperUser | user.Level == AdminLevel.BranchSuperUser) { userNav.Visible = true; }
                if (user.Level == AdminLevel.SuperUser | user.Level == AdminLevel.BranchSuperUser) { glcNav.Visible = true; }
                if (user.Level == AdminLevel.SuperUser | user.Level == AdminLevel.BranchSuperUser) { glaNav.Visible = true; }
                if (user.Level == AdminLevel.SuperUser | user.Level == AdminLevel.BranchSuperUser) { accTypeNav.Visible = true; }
                if (user.Level == AdminLevel.SuperUser | user.Level == AdminLevel.BranchSuperUser) { tellerNav.Visible = true; }

                //Display Financial Date
                DateTime finDate = new Repository<FinancialDate>().GetByID(1).CurrentFinancialDate;
                currentFinancialDate.InnerHtml = finDate.ToShortDateString();
               
            }
            else
            {
                Response.Redirect("../LoginView/login.aspx");
            }
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("../LoginView/login.aspx");
        }
    }
}